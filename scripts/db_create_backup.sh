#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

WORK_DIR=`mktemp -d`
echo "Work directory: $WORK_DIR"

# check if tmp dir was created
if [[ ! "$WORK_DIR" || ! -d "$WORK_DIR" ]]; then
  echo "Could not create temp dir"
  exit 1
fi

# deletes the temp directory
function cleanup {      
  rm -rf "$WORK_DIR"
  echo "Deleted temp working directory $WORK_DIR"
}

# register the cleanup function to be called on the EXIT signal
trap cleanup EXIT

mysqldump autocrypto --user='root' --password='root' > $WORK_DIR/db_backup.sql 

echo "Compressing backup ..."
$(
    cd $WORK_DIR
    tar -cf $SCRIPTPATH/../db_backup/db_backup.tar.gz db_backup.sql -I "pigz -9"
)

echo "Backup created: db_backup/db_backup.tar.gz"
