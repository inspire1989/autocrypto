from modules.misc.logger import logger
import math


### TODO: INTERFACE DEPRECATED ###
class BullBearSumLevDiff():
    def __init__(self) -> None:
        pass

    def get_best_token(self, tickers, token_informations, order_books):

        ### Sum of leverage difference for BULL/BEAR tokens, order by summarized leverage difference ###
        selected_tokens = [
            token for token in token_informations if math.isclose(token["leverage"], 3.0, rel_tol=0.001) or math.isclose(token["leverage"], -3.0, rel_tol=0.001)]

        summarized_lev_diff_results = {}
        for token in selected_tokens:
            # cut "BULL"/"BEAR" from the end
            underlying_symbol = token["name"][:-4]
            if underlying_symbol == "":
                underlying_symbol = "BTC"

            if underlying_symbol + "-PERP" not in summarized_lev_diff_results.keys():
                lev_diff_bull = 0
                if (underlying_symbol if underlying_symbol != "BTC" else "") + "BULL" in [token["name"] for token in selected_tokens]:
                    lev_diff_bull = [(symbol_information["leverage"] - symbol_information["currentLeverage"])
                                     for symbol_information in selected_tokens if symbol_information["name"] == (underlying_symbol if underlying_symbol != "BTC" else "") + "BULL"][0]
                else:
                    logger.warning("Could not find token " +
                                   (underlying_symbol if underlying_symbol != "BTC" else "") + "BULL in selected_tokens")

                lev_diff_bear = 0
                if (underlying_symbol if underlying_symbol != "BTC" else "") + "BEAR" in [token["name"] for token in selected_tokens]:
                    lev_diff_bear = [(symbol_information["leverage"] - symbol_information["currentLeverage"])
                                     for symbol_information in selected_tokens if symbol_information["name"] == (underlying_symbol if underlying_symbol != "BTC" else "") + "BEAR"][0]
                else:
                    logger.warning("Could not find token " +
                                   (underlying_symbol if underlying_symbol != "BTC" else "") + "BEAR in selected_tokens")

                lev_diff = lev_diff_bull - lev_diff_bear

                summarized_lev_diff_results[underlying_symbol +
                                            "-PERP"] = lev_diff

        if len(summarized_lev_diff_results) == 0:
            logger.error("No token information found")
            return None
        else:
            # sort results leverage difference
            summarized_lev_diff_results = {k: v for k, v in sorted(
                summarized_lev_diff_results.items(), key=lambda item: abs(item[1]), reverse=True)}

            logger.debug(
                "Sum of leverage difference for BULL/BEAR tokens, order by summarized leverage difference:")
            for token, summarized_lev_diff in summarized_lev_diff_results.items():
                logger.debug(
                    "- " + token + ": " + str(summarized_lev_diff))

            return summarized_lev_diff_results[0]

    def on_tickers(self, tickers):
        pass

    def on_token_informations(self, token_informations):
        pass

    def on_order_books(self, order_books):
        pass

    def on_ohlcvs(self, ohlcvs):
        pass
