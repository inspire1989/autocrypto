
from modules.misc.Order import Order


class StartStrategyParams():
    def __init__(self, tickers, token_informations, order_books) -> None:
        self.tickers = tickers
        self.token_informations = token_informations
        self.order_books = order_books


class StartStrategyResult():
    def __init__(self, chosen_token, order: Order) -> None:
        self.chosen_token = chosen_token
        self.order = order


class StartStrategyInterface():
    def get_start(self, params: StartStrategyParams) -> StartStrategyResult:
        pass
