import math
from modules.misc.Order import Order, OrderType
from strategies.start.StartStrategyInterface import StartStrategyInterface, StartStrategyParams, StartStrategyResult
from modules.misc.overrides import overrides
from modules.misc.logger import logger
import config.rebalancing_symbols as rebalancing_symbols


class BullLevDiffStartStrategy(StartStrategyInterface):

    def find_lev_difference(self, params: StartStrategyParams, metrics: dict, show_stats_for: str):
        ### For each token, add a metric that describes how good the leverage difference is ###
        tokens_lev_diff = [
            token for token in params.token_informations if token["name"].endswith("BULL")]

        # sort results leverage difference: current leverage - leverage
        tokens_lev_diff.sort(key=lambda k:
                             abs(k['currentLeverage']-k['leverage']), reverse=True)

        max_lev_diff_token = tokens_lev_diff[0]
        logger.info("Token with maximum leverage difference is " + max_lev_diff_token["name"] + ": current leverage " + str(max_lev_diff_token["currentLeverage"]) + ", target leverage " + str(max_lev_diff_token["leverage"]) +
                    ", leverage difference " + str(max_lev_diff_token["currentLeverage"]-max_lev_diff_token["leverage"]))

        # leverage difference metric: normalize to token with highest leverage difference. Result is between -100..100.
        for token in tokens_lev_diff:
            # if current leverage is too close to the target leverage, make this metric "0"
            if math.isclose(token['currentLeverage'], token['leverage'], abs_tol=0.1):
                metrics[token["name"]]["metrics"]["lev_diff"] = 0
            else:
                metrics[token["name"]]["metrics"]["lev_diff"] = (token['currentLeverage']-token['leverage']) / abs(
                    max_lev_diff_token['currentLeverage']-max_lev_diff_token['leverage']) * 100

            if show_stats_for:
                if token["underlying"] == show_stats_for[1]:
                    print("Current leverage: " +
                          str(token['currentLeverage']) + ", target leverage: " + str(token['leverage']))
                    print("Leverage difference: " +
                          str(token['currentLeverage'] - token['leverage']))

    def find_change_bod(self, params: StartStrategyParams, metrics: dict, show_stats_for: str):
        ### For each underlying token, add a metric that describes in which direction the changeBod points (change since 0:00 UTC) ###
        underlying_change_bod = [
            token for token in params.token_informations if token["name"].endswith("BULL")]

        # sort results abs(changeBod)
        underlying_change_bod.sort(key=lambda k:
                                   abs(k['changeBod']), reverse=True)

        max_bod_change_diff_token = underlying_change_bod[0]
        logger.info("Token with maximum absolute bodChange is " +
                    max_bod_change_diff_token["name"] + ": " + str(max_bod_change_diff_token["changeBod"]))

        # changeBod metric: normalize to token with changeBod. Result is between -100..100.
        for token in underlying_change_bod:
            # if current leverage is too close to the target leverage, make this metric "0"
            metrics[token["name"]]["metrics"]["change_bod"] = token['changeBod'] / \
                abs(max_bod_change_diff_token['changeBod']) * 100

            if show_stats_for:
                if token["underlying"] == show_stats_for[1]:
                    print("changeBOD: " +
                          str(token['changeBod']))

    def find_order_book(self, params: StartStrategyParams, metrics: dict, show_stats_for: str):
        ### Evaluate order book ###
        order_books = {metric["underlying"]: params.order_books[metric["underlying"]]
                       for token, metric in metrics.items() if metric["underlying"] in params.order_books.keys()}

        for token, metric in metrics.items():
            # order[1] is the size of the order

            order_frame_size = params.tickers[metric["underlying"]
                                              ]["last"]*1
            sum_bids = sum([order[1] for order in order_books[metric["underlying"]]["bids"] if math.isclose(
                order[0], params.tickers[metric["underlying"]]["last"], abs_tol=order_frame_size)])
            sum_asks = sum([order[1]
                            for order in order_books[metric["underlying"]]["asks"] if math.isclose(order[0], params.tickers[metric["underlying"]]["last"], abs_tol=order_frame_size)])

            # sum_bids = sum([order[1]
            #                 for order in order_books[metric["underlying"]]["bids"]])
            # sum_asks = sum([order[1]
            #                 for order in order_books[metric["underlying"]]["asks"]])

            if sum_bids == 0 or sum_asks == 0:
                metric["metrics"]["order_book"] = 0
                continue

            if sum_bids > sum_asks:
                metric["metrics"]["order_book"] = sum_bids / sum_asks * 100
            else:
                metric["metrics"]["order_book"] = sum_asks / \
                    sum_bids * 100 * -1.0

            if show_stats_for:
                if metric["underlying"] == show_stats_for[1]:
                    print("Sum asks: " + str(sum_asks))
                    print("Sum bids: " + str(sum_bids))
                    print("order_book metric: " +
                          str(metric["metrics"]["order_book"]))

        if show_stats_for:
            print(order_books[show_stats_for[1]])

        # normalize order book metric
        max_order_book_metric = 0
        max_order_book_token = None
        min_order_book_metric = 0
        min_order_book_token = None

        for token_name, metric in metrics.items():
            if metric["metrics"]["order_book"] > max_order_book_metric:
                max_order_book_metric = metric["metrics"]["order_book"]
                max_order_book_token = metric["underlying"]

            if metric["metrics"]["order_book"] < min_order_book_metric:
                min_order_book_metric = metric["metrics"]["order_book"]
                min_order_book_token = metric["underlying"]

        if max_order_book_metric > abs(min_order_book_metric):
            for token_name, metric in metrics.items():
                metric["metrics"]["order_book"] = metric["metrics"]["order_book"] / \
                    max_order_book_metric * 100

            logger.info("Underlying token with maximum absolute order book metric is " +
                        max_order_book_token + ": " + str(max_order_book_metric))
        else:
            for token_name, metric in metrics.items():
                metric["metrics"]["order_book"] = metric["metrics"]["order_book"] / \
                    abs(min_order_book_metric) * 100

            logger.info("Underlying token with maximum absolute order book metric is " +
                        min_order_book_token + ": " + str(min_order_book_metric))

    @overrides(StartStrategyInterface)
    def get_start(self, params: StartStrategyParams) -> StartStrategyResult:

        show_stats_for = None
        # show_stats_for = ["ZECBULL", "ZEC-PERP"]

        control = {"lev_diff": True,
                   "change_bod": True,
                   "order_book": True
                   }

        ### Set up metrics dict for each BULL token ###
        if rebalancing_symbols.whitelist_enabled:
            metrics = {token["name"]: {"underlying": token["underlying"], "metrics": {}}
                       for token in params.token_informations if (token["name"].endswith("BULL")) and (token["underlying"] in rebalancing_symbols.symbol_whitelist)}
        else:
            metrics = {token["name"]: {"underlying": token["underlying"], "metrics": {}}
                       for token in params.token_informations if token["name"].endswith("BULL")}

        self.find_lev_difference(params, metrics, show_stats_for)
        self.find_change_bod(params, metrics, show_stats_for)
        self.find_order_book(params, metrics, show_stats_for)

        ### Calculate sum of all metrics (can be negative!). Set sum to 0 if single metrics point into different directions (positive and negative ones present) ###
        for token_name, values in metrics.items():
            values["metric_sum"] = 0

            direction = None  # "long"/"short"
            for metric_name, value in values["metrics"].items():

                # if the metric isn't enabled, don't count it for the metric_sum
                if control[metric_name] == False:
                    continue

                if value == 0:  # previous algorithms could set value to 0 in order to block using it
                    values["metric_sum"] = 0
                    break

                if not direction:
                    direction = ("long" if value > 0 else "short")
                else:
                    new_direction = ("long" if value > 0 else "short")

                    # metrics point into different directions --> set sum to 0 and stop here ("got mixed signals...")
                    if new_direction != direction:
                        if (direction == "long" and value < -10) or (direction == "short" and value > 10):
                            values["metric_sum"] = 0
                            break

                if metric_name == "lev_diff":
                    if control["lev_diff"]:
                        values["metric_sum"] += value*1
                elif metric_name == "order_book":
                    if control["order_book"]:
                        values["metric_sum"] += value*1
                elif metric_name == "change_bod":
                    if control["change_bod"]:
                        values["metric_sum"] += value*1
                else:
                    raise Exception("No weight for metric " + metric_name)

        if show_stats_for:
            print(metrics[show_stats_for[0]])

        ### Select the token with highest absolute metrics_sum ###
        max_abs_metric_sum = 0
        max_abs_metric_token = None
        for token_name, values in metrics.items():
            if abs(values["metric_sum"]) > abs(max_abs_metric_sum):
                max_abs_metric_sum = values["metric_sum"]
                max_abs_metric_token = token_name

        # in case the metric sums of all tokens are 0, no good token candidate was found
        if max_abs_metric_sum == 0:
            logger.warning("No token found that suits...")
            return StartStrategyResult(chosen_token=None, order=None)

        logger.info("Token with maximum absolute metric sum is " +
                    max_abs_metric_token + ": " + str(metrics[max_abs_metric_token]))

        underlying_token = [token["underlying"]
                            for token in params.token_informations if token["name"] == max_abs_metric_token]
        if len(underlying_token) != 1:
            raise Exception("Ooops, data corrupt...")
        underlying_token = underlying_token[0]

        if metrics[max_abs_metric_token]["metric_sum"] > 0:
            order = Order(symbol=underlying_token,
                          order_type=OrderType.LONG,
                          leverage=5)
        elif metrics[max_abs_metric_token]["metric_sum"] < 0:
            order = Order(symbol=underlying_token,
                          order_type=OrderType.SHORT,
                          leverage=5)
        else:
            raise Exception("Chosen token has metric_sum=0")

        return StartStrategyResult(chosen_token=max_abs_metric_token, order=order)
