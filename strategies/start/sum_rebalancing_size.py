from modules.misc.logger import logger
import math


### TODO: INTERFACE DEPRECATED ###
class SumRebalancingSize():
    def __init__(self) -> None:
        pass

    def get_best_token(self, tickers, token_informations, order_books):

        for token_information in token_informations:
            # see https://help.ftx.com/hc/en-us/articles/360032720752-How-Do-Leveraged-Tokens-Rebalance: "Rebalance size: (DP - CP)"
            token_information["rebalancingSize"] = (token_information['leverage']*token_information['totalNav']/token_information["underlyingPrice"])-(
                token_information["outstanding"] * token_information["basketUnderlying"])

        ### Sum of rebalancingSize for BULL/BEAR tokens, order by summarized rebalancingSize ###
        selected_tokens = [
            token for token in token_informations if math.isclose(token["leverage"], 3.0, rel_tol=0.001) or math.isclose(token["leverage"], -3.0, rel_tol=0.001)]

        summarized_rebalancing_size_results = {}
        for token in selected_tokens:
            # cut "BULL"/"BEAR" from the end
            underlying_symbol = token["name"][:-4]
            if underlying_symbol == "":
                underlying_symbol = "BTC"

            if underlying_symbol + "-PERP" not in summarized_rebalancing_size_results.keys():
                rebalancing_size_bull = 0
                if (underlying_symbol if underlying_symbol != "BTC" else "") + "BULL" in [token["name"] for token in selected_tokens]:
                    rebalancing_size_bull = [symbol_information["rebalancingSize"]
                                             for symbol_information in selected_tokens if symbol_information["name"] == (underlying_symbol if underlying_symbol != "BTC" else "") + "BULL"][0]
                else:
                    logger.warning("Could not find token " +
                                   (underlying_symbol if underlying_symbol != "BTC" else "") + "BULL in selected_tokens")

                rebalancing_size_bear = 0
                if (underlying_symbol if underlying_symbol != "BTC" else "") + "BEAR" in [token["name"] for token in selected_tokens]:
                    rebalancing_size_bear = [symbol_information["rebalancingSize"]
                                             for symbol_information in selected_tokens if symbol_information["name"] == (underlying_symbol if underlying_symbol != "BTC" else "") + "BEAR"][0]
                else:
                    logger.warning("Could not find token " +
                                   (underlying_symbol if underlying_symbol != "BTC" else "") + "BEAR in selected_tokens")

                summarized_rebalancing_size = rebalancing_size_bull - rebalancing_size_bear

                summarized_rebalancing_size_results[underlying_symbol +
                                                    "-PERP"] = summarized_rebalancing_size

        if len(summarized_rebalancing_size_results) == 0:
            logger.error("No token information found")
            return None
        else:
            # sort results by summarized rebalancing size
            summarized_rebalancing_size_results = {k: v for k, v in sorted(
                summarized_rebalancing_size_results.items(), key=lambda item: abs(item[1]), reverse=True)}

            logger.debug(
                "Sum of rebalancingSize for BULL/BEAR tokens, order by summarized rebalancingSize:")
            for token, summarized_rebalancing_size in summarized_rebalancing_size_results.items():
                logger.debug(
                    "- " + token + ": " + str(summarized_rebalancing_size))

            return summarized_rebalancing_size_results[0]

    def on_tickers(self, tickers):
        pass

    def on_token_informations(self, token_informations):
        pass

    def on_order_books(self, order_books):
        pass

    def on_ohlcvs(self, ohlcvs):
        pass
