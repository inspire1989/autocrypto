import math
from strategies.stop.StopStrategyInterface import StopStrategyInterface, StopStrategyParams
from modules.misc.overrides import overrides
from modules.misc.logger import logger


class TargetLeverageReachedStopStrategy(StopStrategyInterface):

    @overrides(StopStrategyInterface)
    def check(self, stop_strategy_params: StopStrategyParams) -> bool:
        ### 1. Find chosen token ###
        information_for_chosen_token = [
            token for token in stop_strategy_params.token_informations if token["name"] == stop_strategy_params.chosen_token]

        if len(information_for_chosen_token) != 1:
            logger.warning("No token information found")
            return False

        information_for_chosen_token = information_for_chosen_token[0]
        if math.isclose(information_for_chosen_token["currentLeverage"], information_for_chosen_token["leverage"], rel_tol=0.05):
            logger.info("Target leverage reached")
            return True

        return False
