
class StopStrategyParams():
    def __init__(self, rebalancing_sniper_start_time, chosen_token, order, tickers, token_informations, order_books) -> None:
        self.rebalancing_sniper_start_time = rebalancing_sniper_start_time
        self.chosen_token = chosen_token
        self.order = order
        self.tickers = tickers
        self.token_informations = token_informations
        self.order_books = order_books


class StopStrategyInterface():
    def check(self, params: StopStrategyParams) -> bool:
        pass
