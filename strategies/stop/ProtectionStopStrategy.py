from modules.misc.Order import OrderType
from modules.misc.Exceptions import TooLateException
from modules.misc.misc import verify_time_is_before
from strategies.stop.StopStrategyInterface import StopStrategyInterface, StopStrategyParams
from modules.misc.overrides import overrides
from modules.misc.logger import logger


class ProtectionStopStrategy(StopStrategyInterface):

    @overrides(StopStrategyInterface)
    def check(self, params: StopStrategyParams) -> bool:
        try:
            verify_time_is_before(params.rebalancing_sniper_start_time.replace(
                minute=4, second=0, microsecond=0))
        except TooLateException as e:
            logger.warning(
                "Protection stop strategy triggered at " + str(e.verify_timestamp))
            return True

        # stop loss at 1/(leverage*10) difference from open price
        tickers_not_found_counter = 0
        if params.tickers:
            tickers_not_found_counter = 0
            if params.order.symbol in params.tickers:
                if params.order.order_type == OrderType.LONG:
                    if params.tickers[params.order.symbol]["last"] <= params.order.open_price*(1-1/(params.order.leverage*10)):
                        logger.warning("SL hit")
                        return True
                elif params.order.order_type == OrderType.SHORT:
                    if params.tickers[params.order.symbol]["last"] >= params.order.open_price*(1+1/(params.order.leverage*10)):
                        logger.warning("SL hit")
                        return True
        else:
            tickers_not_found_counter += 1
            if tickers_not_found_counter == 3:
                logger.warning(
                    "Tickers not found 3 times in a row, closing position")
                return True

        return False
