#!/usr/bin/env python3

import modules.misc.mariadb_interface as mariadb_interface


def fix_json_format():
    mariadb_interface.db.cursor().execute(
        """UPDATE rebalancing_raw SET value = REPLACE(value, '''', '"'); """)
    mariadb_interface.db.cursor().execute(
        "UPDATE rebalancing_raw SET value = REPLACE(value, ': None', ': null');")
    mariadb_interface.db.cursor().execute(
        "UPDATE rebalancing_raw SET value = REPLACE(value, ':None', ': null');")
    mariadb_interface.db.cursor().execute(
        "UPDATE rebalancing_raw SET value = REPLACE(value, ': True', ': true');")
    mariadb_interface.db.cursor().execute(
        "UPDATE rebalancing_raw SET value = REPLACE(value, ':True', ': true');")
    mariadb_interface.db.cursor().execute(
        "UPDATE rebalancing_raw SET value = REPLACE(value, ': False', ': false');")
    mariadb_interface.db.cursor().execute(
        "UPDATE rebalancing_raw SET value = REPLACE(value, ':False', ': false');")
    mariadb_interface.db.commit()


if __name__ == "__main__":
    mariadb_interface.connect_db()

    fix_json_format()

    mariadb_interface.disconnect_db()

    print("Finished")
