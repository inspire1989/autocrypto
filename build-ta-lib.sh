#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd ta-lib-0.4.0

./configure --prefix=$SCRIPTPATH/talib-build/$(arch)

make

make install
