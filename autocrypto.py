#!/usr/bin/env python3

import argparse
import logging
import sys
from modules.misc.logger import logger, CustomFormatter
import modules.misc.mode_selector as mode_selector
import modules.simulation.simulation as simulation


def parse_args():
    parser = argparse.ArgumentParser(conflict_handler='resolve')
    parser.add_argument("mode", choices=['binance-download', 'run', 'whale-download',
                        'whale-show', 'rebalancing', 'record', 'analyze', 'test'], help="Autocrypto mode")

    binance_download_group = parser.add_argument_group("Binance download mode")
    binance_download_group.add_argument(
        "--coin", dest="coin", help="Coin used for download. E.g. ETHBTC")
    binance_download_group.add_argument("--interval", dest="interval",
                                        help="Interval for data: 1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 8h, 12h, 1d, 3d, 1w, 1M.")
    binance_download_group.add_argument(
        "--start", dest="start", help="Start time for data download. UTC format or timestamp in milliseconds.")
    binance_download_group.add_argument(
        "--end", dest="end", help="End time for data download. UTC format or timestamp in milliseconds.")
    binance_download_group.add_argument(
        "--show", dest="show", help="Show the data.", action="store_true")

    whale_download_group = parser.add_argument_group("whale-download mode")
    whale_download_group.add_argument(
        "--start", dest="start", help="Start time for data download. UTC format.")
    whale_download_group.add_argument(
        "--end", dest="end", help="End time for data download. UTC format.")
    whale_download_group.add_argument(
        "--file", dest="file", help="Extend whale data in this file.")
    whale_download_group.add_argument(
        "--currency", dest="currency", help="Filter for currency like btc.")

    whale_show_group = parser.add_argument_group("whale-show mode")
    whale_show_group.add_argument(
        "--file", dest="file", help="Show whale data from this file.")

    rebalancing_group = parser.add_argument_group("rebalancing mode")
    rebalancing_group.add_argument(
        "--sniper", help="Sniper mode.", action="store_true")
    rebalancing_group.add_argument(
        "--capture", help="Capture mode.", action="store_true")
    rebalancing_group.add_argument(
        "--analyze", help="Analyze mode.", action="store_true")

    rebalancing_group_optional = parser.add_argument_group("Optional flags")
    rebalancing_group_optional.add_argument(
        "--test", "-t", dest="test", help="Test mode. Program wont't wait for the next rebalancing.", action="store_true")
    rebalancing_group_optional.add_argument(
        "--simulate", "-s", dest="simulate", help="Simulation mode. Runs on previously captured data.", action="store_true")

    rebalancing_analysis_group = parser.add_argument_group(
        "rebalancing-analysis mode")

    test_group = parser.add_argument_group("test mode")
    test_group.add_argument(
        "--file", dest="file", help="Binance csv file.")

    parser.add_argument("--verbose", "-v", dest="verbose",
                        help="Verbose mode", action="store_true")

    return parser, parser.parse_args()


def init_logging(args):
    logger.setLevel((logging.DEBUG if args.verbose else logging.INFO))
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel((logging.DEBUG if args.verbose else logging.INFO))
    formatter = CustomFormatter()
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    if args.simulate:
        def override_sim_time(self, record, datefmt=None):
            if simulation.sim_time:
                return simulation.sim_time.strftime('%Y-%m-%d %H:%M:%S,%f')[:-3]
            else:
                return simulation.sim_day.strftime('%Y-%m-%d %H:%M:%S,%f')[:-3]

        logging.Formatter.formatTime = override_sim_time


def main(parser, args):
    init_logging(args)

    # set test mode if given by command line parameter
    if args.simulate:
        mode_selector.simulation_mode = True
        logger.warning("SIMULATION MODE ACTIVATED")
    elif args.test:
        mode_selector.test_mode = True
        logger.warning("TEST MODE ACTIVATED")

    if args.mode == "binance-download":
        from config.BinanceConfig import BinanceConfig
        config = BinanceConfig()

        from modules.binance_download import binance_download
        binance_download(config, parser, args)
    elif args.mode == "run":
        logger.error("run mode not implemented")
    elif args.mode == "whale-download":
        from config.WhaleAlertConfig import WhaleAlertConfig
        config = WhaleAlertConfig()

        from modules.whale_download import whale_download
        whale_download(config, parser, args)
    elif args.mode == "whale-show":
        from config.WhaleAlertConfig import WhaleAlertConfig
        config = WhaleAlertConfig()

        from modules.whale_show import whale_show
        whale_show(config, parser, args)
    elif args.mode == "rebalancing":
        if args.sniper or args.capture:
            from config.ccxtConfig import ccxtConfig
            config = ccxtConfig()

            from modules.rebalancing import rebalancing
            rebalancing(config, parser, args)
        elif args.analyze:
            from modules.rebalancing_analysis import rebalancing_analysis
            rebalancing_analysis(parser, args)
    elif args.mode == "record":
        from config.ccxtConfig import ccxtConfig
        config = ccxtConfig()

        from modules.record import record
        record(config, parser, args)
    elif args.mode == "analyze":
        from modules.analyze import analyze
        analyze(parser, args)
    elif args.mode == "test":
        from modules.test import test
        test(parser, args)
    else:
        logger.critical("Unknown mode!")


if __name__ == "__main__":
    current_parser, current_args = parse_args()
    main(current_parser, current_args)
