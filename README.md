# AutoCrypto

Project for crypto analytics and bot algorithms.

# Setup

Install some tools:

```bash
sudo apt install git-lfs pigz
git lfs install
```

## MariaDB

First, install MariaDB:

```bash
sudo apt-get update
sudo apt install mariadb-server libmariadb3 libmariadb-dev
sudo mysql_secure_installation
```

Log in to mariadb with the root user initially:

```bash
sudo mysql -u root -p
```

and then run these commands to
- allow accessing mariadb with user permissions
- improve performance

```
set global thread_cache_size = 20;
GRANT ALL PRIVILEGES on *.* to 'root'@'localhost' IDENTIFIED BY 'root';
FLUSH PRIVILEGES;
```

Type in `exit`.

Also, increase the size of allowed packets in the db with

```bash
echo "max_allowed_packet=1G" | sudo tee -a /etc/mysql/my.cnf
```

Then increase the linux file limit by appending two lines to /etc/security/limits.conf using this command:

```bash
cat /etc/security/limits.conf | head -n -1  && echo -e "\nmysql soft nofile 65535\nmysql hard nofile 65535\n\n# End of file" sudo tee /etc/security/limits.conf
```

Then restart the service:

```bash
sudo service mysql restart
```

Afterwards you can access the server with 

```bash
mysql -u root -p
```

For db analysis, install the tool DBeaver: https://dbeaver.io/download/. It works on both Windows and Linux and can connect to a VM over an ssh tunnel.


## Python environment
### CPython
Install python:

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install python3.8 python3.8-venv python3.8-distutils python3.8-dev
```

Create virtual environment and set it up:

```bash
python3.8 -m venv env
```

### PyPy

**CURRENTLY BROKEN**

Install python:

```bash
wget -P /tmp https://buildbot.pypy.org/nightly/py3.7/pypy-c-jit-102707-9c0e0671120f-linux64.tar.bz2
tar -xf /tmp/pypy-c-jit-102707-9c0e0671120f-linux64.tar.bz2 -C $HOME
$HOME/pypy-c-jit-102707-9c0e0671120f-linux64/bin/pypy -m ensurepip -U
/home/martin/pypy-c-jit-102707-9c0e0671120f-linux64/bin/pypy -mpip install -U pip wheel
```

Create virtual environment and set it up:

```bash
virtualenv -p $HOME/pypy-c-jit-102707-9c0e0671120f-linux64/bin/pypy envpypy

source envpypy/bin/activate
pypy -m ensurepip -U
pypy -mpip install -U pip wheel
```

### Set up the virtual environment
Activate the virtual environment it with

```bash
source env/bin/activate
```

For Windows, use

```bash
.\env\Scripts\activate.bat
```

Set environment variables:

```bash
export TA_LIBRARY_PATH=${PWD}/talib-build/$(arch)/lib
export TA_INCLUDE_PATH=${PWD}/talib-build/$(arch)/include
```

Install wheel:

```bash
pip install wheel
```

Install all required packages:

```bash
pip install -r requirements.txt
```

Freeze requirements with

```bash
pip freeze > requirements.txt
```

# Usage

First, choose your API keys in the `config` folder.

Then, activate the virtual environment:

```bash
source venv/bin/activate
```

Deactivate it with

```bash
deactivate
```

In general, run the `autocrypto.py` script.

Turn on verbose mode with `-v`.

Profile it with `python -m cProfile -s cumulative ./autocrypto [params]`

## Capture token rebalancings for analysis

Captures data from BULL/BEAR/HEDGE token rebalancings (0:02 UTC) and stores in the mariadb in the `rebalancing_raw` table.

```bash
./autocrypto.py rebalancing --capture
```

**Test mode:** Add `-t` parameter. Won't store anything in db.

## Rebalancing sniper trading bot

***Not profitable :< only shows how it can be done...***

Waits until 0:02 UTC, then tries to identify the token that will have it's rebalancing with highest trade volume. The bot will then buy/sell the underlying PERP token (depending on the current leverage). Once rebalancing is finished, the position gets closed.

```bash
./autocrypto.py rebalancing --sniper
```

**Simulation mode:** Add `-s` parameter. Simulates bot behavior based on the data captured by the "--capture" mode.


## Download and visualize Binance data for a coin

```bash
./autocrypto.py binance-download --coin=BTC_USDT --interval=1d --start=2017-01-01 --end=2020-07-09 --show
```

## Download and visualize whale data

With start and end date:

```bash
./autocrypto.py whale-download --start 01.01.2020 --end 31.03.2020
```

Stores the data in the history folder.

Extend an existing file:

```bash
./autocrypto.py whale-download --file ./history/whale-alert-01.01.2020_now.txt
```

## Show previously downloaded whale data

```bash
./autocrypto.py whale-show --file ./history/whale-alert-01.01.2020_now.txt
```

# Build ta-lib

ta-lib is a library that provides tons of indicators that can be used to build crypto trading bots.

Install build utilities for compiling talib:

```bash
sudo apt-get install build-essential
```

Run the script

```bash
./build-ta-lib.sh
```

It will compile ta-lib for the current architecture.

# Fix db entries

The fix-db.py script can be used to update existing data in the db if it's not compatible anymore.
