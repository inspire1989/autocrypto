from modules.misc.logger import logger
import json
import os
from pathlib import Path
import requests
import time
from datetime import datetime

from modules.misc.misc import get_root_folder, sleep_for


def download_transactions(config, start, currency=None, end='now', file=None, min_value=None, limit=100,
                          cursor='2bc7e46-2bc7e46-5c66c0a7'):

    if file:
        logger.info('Extending file ' + file)

        if not os.path.exists(file):
            logger.critical("File " + str(file) + " does not exist.")
            exit(1)

        # find timestamp of last data in the file
        with open(file, 'r', encoding='utf-8', buffering=1) as f:
            for line in f:
                pass
            last_line = line
            last_result = json.loads(last_line)

        start_timestamp = int(last_result['timestamp']) + 1

        logger.info('Last timestamp found in file: ' + str(datetime.fromtimestamp(
            last_result['timestamp']).strftime('%d.%m.%Y %H:%M:%S')))

        request_string = 'https://api.whale-alert.io/v1/transactions?api_key=' + config.api_key + \
                         '&start=' + str(start_timestamp) + \
                         '&end=' + str(int(time.time())) + \
                         ('&min_value=' + min_value if min_value else '') + \
                         ('&currency=' + currency if currency else '') + \
                         ('&limit=' + str(limit) if limit else '')

        file_open_mode = 'a'

    else:
        logger.info('Querying transactions from ' + start + ' until ' +
                    end + (' [' + currency + ']' if currency else ''))

        start_timestamp = str(
            int(time.mktime(datetime.strptime(start, "%d.%m.%Y").timetuple())))

        if end == 'now':
            end_timestamp = str(int(time.time()))
            end_day = datetime.fromtimestamp(time.time()).strftime('%d.%m.%Y')
        else:
            end_day = str(datetime.strptime(end, "%d.%m.%Y"))
            end_timestamp = str(
                int(time.mktime(datetime.strptime(end, "%d.%m.%Y").timetuple())))

        # Save data as text file
        output_folder = Path(get_root_folder()) / Path('history')
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        file = Path(output_folder) / Path('whale-alert-' +
                                          start + '_' + end_day + '.txt')

        if os.path.exists(file):
            logger.critical("Output file " + str(file) + " exists.")
            exit(1)

        request_string = 'https://api.whale-alert.io/v1/transactions?api_key=' + config.api_key + \
                         '&start=' + start_timestamp + \
                         ('&end=' + end_timestamp if end else '') + \
                         ('&min_value=' + min_value if min_value else '') + \
                         ('&currency=' + currency if currency else '') + \
                         ('&limit=' + str(limit) if limit else '')

        file_open_mode = 'w+'

    with open(file, file_open_mode, encoding='utf-8', buffering=1) as f:
        try:
            while True:
                try:
                    resp = requests.get(request_string + '&cursor=' + cursor)
                except:
                    logger.debug('Exception :(')
                    continue

                if resp.status_code != 200:
                    # This means something went wrong.
                    logger.warning(
                        'Error code ' + str(resp.status_code) + " - retrying...")
                    continue
                else:
                    result = resp.json(strict=False)
                    if result['result'] != 'success':
                        logger.warning("Query not successful - retrying...")
                        continue

                    if result['count'] == 0:  # nothing to download anymore
                        logger.debug('Nothing to download anymore')
                        break

                    # store transactions in file
                    for transaction in result['transactions']:
                        json.dump(transaction, f, ensure_ascii=False)
                        f.write('\n')

                if not result['cursor']:
                    logger.debug('Cursor not provided')
                    break

                if cursor == result['cursor']:
                    logger.debug('Cursor didn\'t change')
                    break

                cursor = result['cursor']

                logger.info('- Received transactions until ' + datetime.fromtimestamp(
                    result['transactions'][-1]['timestamp']).strftime('%Y-%m-%d %H:%M:%S'))

                time.sleep(60.0 / config.api_calls_per_minute + 0.1)

        except KeyboardInterrupt:
            logger.critical('Interrupted!')
            exit(1)
        except json.decoder.JSONDecodeError as e:
            logger.critical('JSON decode error: ' + str(e) +
                            '. The JSON string that caused this problem was saved in the file json_error.txt.')

            with open(get_root_folder() + '/json_error.txt', 'w', encoding='utf-8') as f:
                f.write(resp.text)

            exit(1)

    logger.info("Whale data saved at " + str(file))


def get_supported_blockchains(config):
    resp = requests.get(
        'https://api.whale-alert.io/v1/status?api_key=' + config.api_key)
    if resp.status_code != 200:
        # This means something went wrong.
        logger.critical('Error code ' + str(resp.status_code))
        exit(1)
    else:
        result = resp.json()
        if result['result'] != 'success':
            logger.critical("Query failed")
            exit(1)

        logger.info('Supported whale-alert blockchains: ')
        for blockchain in result['blockchains']:
            logger.info('- ' + str(blockchain))


def whale_download(config, parser, args):
    if (args.start is None or args.end is None) and (args.file is None):
        parser.error(
            "Whale-download mode requires start and end time specified with -s START and -e END OR a file with --file.")

    get_supported_blockchains(config)

    logger.info("Downloading whale-alert data")

    download_transactions(config=config, start=args.start,
                          end=args.end, file=args.file, currency=args.currency)
