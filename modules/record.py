from concurrent.futures.thread import ThreadPoolExecutor
from hmac import new

import pause
from modules.misc.Order import Order, OrderType
from strategies.stop.StopStrategyInterface import StopStrategyParams
from strategies.start.StartStrategyInterface import StartStrategyParams
from strategies.stop.TargetLeverageReachedStopStrategy import TargetLeverageReachedStopStrategy
from strategies.stop.ProtectionStopStrategy import ProtectionStopStrategy
from modules.misc.Exceptions import EndOfSimulationDataException

from modules.simulation.simulation import SimulationFinished, get_data_for_day_from_db
from modules.misc.misc import TooLateException, find_symbols_with_etf_tokens, sleep_for, verify_time_is_before, wait_until
from modules.misc.exchange_functions import get_ohlcvs, get_order_books, open_position, close_position, get_balance, get_etf_token_informations, get_ohlcv, get_order_book, get_tickers, init_exchange, market_close_all_positions, open_position
from modules.misc.logger import logger
import config.rebalancing_symbols as rebalancing_symbols
import modules.misc.thread_exception_handler as thread_exception_handler
import modules.misc.mariadb_interface as mariadb_interface
import modules.misc.mode_selector as mode_selector
import modules.simulation.simulation as simulation

from strategies.start.BullLevDiffStartStrategy import BullLevDiffStartStrategy

from datetime import datetime, timedelta
import time
import pytz
import os
import traceback


def emergency_abort():
    mariadb_interface.lock.acquire()  # ensure db is not used currently
    os._exit(1)


def init(config):
    init_exchange(config)

    # install callback for exception handling (within threads only!)
    thread_exception_handler.exception_callback = emergency_abort

    symbols_to_record = ["BTC-PERP", "ETH-PERP", "LINK-PERP", "XRP-PERP",
                         "ADA-PERP", "DOT-PERP", "FTT-PERP", "BNB-PERP", "SXP-PERP"]
    return symbols_to_record


def record(config, parser, args):

    symbols_to_record = init(config)

    logger.info("Recording starts for these symbols: " +
                str(symbols_to_record))

    mariadb_interface.connect_db()

    counter = 0
    now = datetime.utcnow().replace(tzinfo=pytz.UTC).replace(second=0, microsecond=0)

    try:
        while True:
            print()

            try:
                logger.info("Pausing until " +
                            str(now + timedelta(minutes=1) * (counter+1)))
                pause.until(now + timedelta(minutes=1) * (counter+1))

                get_tickers(update_db=True, table="recording")

                get_order_books(update_db=True, table="recording",
                                symbols=symbols_to_record, depth=100, grouping=1)
                get_order_books(update_db=True, table="recording",
                                symbols=symbols_to_record, depth=100, grouping=5)
                get_order_books(update_db=True, table="recording",
                                symbols=symbols_to_record, depth=100, grouping=10)
                get_order_books(update_db=True, table="recording",
                                symbols=symbols_to_record, depth=100, grouping=50)
                get_order_books(update_db=True, table="recording",
                                symbols=symbols_to_record, depth=100, grouping=100)
                get_order_books(update_db=True, table="recording",
                                symbols=symbols_to_record, depth=100, grouping=500)

                get_ohlcvs(symbols=symbols_to_record,
                           update_db=True,
                           table="recording", timeframe="1m")

                if datetime.utcnow().minute % 5 == 0:
                    get_ohlcvs(symbols=symbols_to_record,
                               update_db=True,
                               table="recording", timeframe="5m")

                counter += 1
            except KeyboardInterrupt:  # completely abort
                traceback.print_exc()
                raise KeyboardInterrupt()

            except Exception as e:
                logger.error("Caught exception: " + str(e) +
                             ". Aborting current day's rebalancing.")
                traceback.print_exc()
                emergency_abort()
    except KeyboardInterrupt:
        logger.warning("KeyboardInterrupt")
        if args.sniper:
            logger.critical("Open positions on exchange haven't been closed!")
    except Exception:
        logger.critical(traceback.format_exc())
        emergency_abort()
    finally:
        mariadb_interface.disconnect_db()
