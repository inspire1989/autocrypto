
def overrides(interface_class):
    """
    Provides an "overrides" decorator. Ensures the method that shall be overridden exists in the parent class. Use it like this:

    class Parent():
        def method(self, foo, bar):
            pass

    class Child(Parent):
        @overrides(Parent)
        def method(self, foo, bar):
            pass
    """
    def overrider(method):
        assert method.__name__ in dir(interface_class), \
            "Interface class " + str(interface_class.__name__) + \
            " doesn't define the method \"" + \
            str(method.__name__) + "\" although it is marked as \"overrides\"."
        return method
    return overrider
