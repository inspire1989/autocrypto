import logging

# Use this logger instance everywhere
logger = logging.getLogger(__name__)


class CustomFormatter(logging.Formatter):
    """
    Logging Formatter to add colors.
    """

    green = "\x1b[0;32m"
    white = "\x1b[0m"
    yellow = "\x1b[0;33m"
    red = "\x1b[0;31m"
    bold_red = "\x1b[1;31m"
    reset = "\x1b[0m"
    #format = "[%(levelname)s] %(asctime)s [%(filename)s:%(lineno)d] %(message)s"
    format = "[%(filename)s:%(lineno)d] %(message)s"

    FORMATS = {
        logging.DEBUG: green + format + reset,
        logging.INFO: white + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)
