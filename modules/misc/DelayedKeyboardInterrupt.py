import signal
import logging


class DelayedKeyboardInterrupt(object):
    """
    Ensures that a section of code cannot be interrupted by the KeyboardInterrupt.
    Use it like this:
        with DelayedKeyboardInterrupt:
            perform_operations_that_cannot_be_interrupted()
    """

    def __enter__(self):
        self.signal_received = False
        self.old_handler = signal.signal(signal.SIGINT, self.handler)

    def handler(self, sig, frame):
        self.signal_received = (sig, frame)
        logging.debug('SIGINT received. Delaying KeyboardInterrupt.')

    def __exit__(self, type, value, traceback):
        signal.signal(signal.SIGINT, self.old_handler)
        if self.signal_received:
            self.old_handler(*self.signal_received)
