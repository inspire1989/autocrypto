from array import array
from modules.misc.Order import Order, OrderType
import modules.simulation.simulation as simulation
from modules.simulation.simulation import get_next_etf_token_informations, get_next_order_book, get_next_order_books, get_next_tickers
from modules.misc.logger import logger
import modules.misc.mariadb_interface as mariadb_interface
import modules.misc.thread_exception_handler as thread_exception_handler
import modules.misc.mode_selector as mode_selector
import config.rebalancing_symbols as rebalancing_symbols

from datetime import datetime, timedelta
from concurrent.futures.thread import ThreadPoolExecutor
import threading
import traceback
import pytz
import ccxt
import math
import requests
import orjson
import time
import hmac

global ftx
ftx = None

global makerFee
global takerFee
makerFee = None
takerFee = None


def init_exchange(config):
    """
    Initializes the ccxt library.
    :param config Config to use
    """
    global ftx
    ftx = ccxt.ftx(
        {
            "apiKey": config.apiKey,
            "secret": config.secret,
            "timeout": 15000
        }
    )

    # get fees
    ts = int(time.time() * 1000)
    request = requests.Request('GET', 'https://ftx.com/api/account')
    prepared = request.prepare()
    signature_payload = f'{ts}{prepared.method}{prepared.path_url}'.encode()
    signature = hmac.new(config.secret.encode(),
                         signature_payload, 'sha256').hexdigest()

    request = requests.get("https://ftx.com/api/account", headers={
                           'FTX-KEY': config.apiKey, 'FTX-SIGN': signature, 'FTX-TS': str(ts)}, timeout=15)

    # raise Exception in case request didn't succeed with HTTP 200
    request.raise_for_status()

    json_data = orjson.loads(request.text)

    global makerFee
    global takerFee
    makerFee = json_data["result"]["makerFee"]
    takerFee = json_data["result"]["takerFee"]

    logger.debug("Maker fee: " + str(makerFee))
    logger.debug("Taker fee: " + str(takerFee))


def get_balance(update_db=True):
    """
    Get the current total balance from the exchange and store it in the db in the "balance" table.
    :param update_db: True if db shall be updated, False if not.
    :return total balance in USD
    """

    if mode_selector.simulation_mode:
        total_balance = simulation.balance
        logger.debug("Simulated balance: $" + str(total_balance))
    else:
        try:
            # show current total balance
            total_balance = sum(
                [float(coin['usdValue'])
                    for coin in ftx.fetch_balance()['info']['result']]
            )

            if update_db:
                mariadb_interface.db_insert_balance(
                    table='balance', exchange='ftx', symbol='total', timestamp=datetime.utcnow(), value=total_balance)
        except Exception as e:
            logger.critical(e)
            logger.critical(traceback.format_exc())

    return total_balance


def get_tickers(update_db=False, max_sim_time_increase=timedelta(seconds=5), table=None):
    """
    Get the ticker (latest prices for all symbols at once).
    :param update_db: True if db entry shall be made.
    :param max_sim_time_increase: (Simulation mode only) Max timedelta from now where data shall be searched for (5 seconds default). If no data was found in this timedelta, None is returned.
    :return current price in USD
    """
    result = None
    logger.debug("Fetching tickers")
    if mode_selector.simulation_mode:
        result = get_next_tickers(max_sim_time_increase)
    else:
        try:
            result = ftx.fetch_tickers()

            if update_db:
                mariadb_interface.db_insert_raw(table=table, exchange="ftx", timestamp=datetime.utcnow(
                ).replace(tzinfo=pytz.UTC), information_type="tickers", value=str(result))
        except Exception as e:
            logger.critical(e)
            logger.critical(traceback.format_exc())

    return result


def get_etf_token_informations(symbols, tickers, update_db=False, max_sim_time_increase=timedelta(seconds=5)):
    """
    Get information about all symbols and order it by leverage difference to is's target leverage.
    :param symbols List of all symbols to check
    :param update_db: True if db entry shall be made.
    :param max_sim_time_increase: (Simulation mode only) Max timedelta from now where data shall be searched for (5 seconds default). If no data was found in this timedelta, None is returned.
    :return Dictionary of symbol data that has the highest leverage difference to it's target leverage
    """
    logger.debug("Fetching ETF token information")
    if mode_selector.simulation_mode:
        return get_next_etf_token_informations(max_sim_time_increase)
    else:
        try:
            result = []

            r = requests.get("https://ftx.com/api/lt/tokens", timeout=15)

            r.raise_for_status()  # raise Exception in case request didn't succeed with HTTP 200

            # use orjson here because it can properly print json objects/arrays to a string (in contrast to cysimdjson) and it's still really fast
            json_data = orjson.loads(r.text)

            if json_data["success"] != True:
                raise Exception("Could not retrieve rebalancings")

            if len(json_data["result"]) == 0:
                raise Exception("No results found in json")

            for token_information in json_data["result"]:
                if token_information["name"] not in symbols:
                    continue

                # few tokens have currentLeverage == 0 ... skip these
                if math.isclose(token_information["currentLeverage"], 0, rel_tol=0.00001):
                    continue

                if not tickers:
                    logger.warning("No tickers provided")

                if token_information["underlying"] in tickers:
                    underlying_price = tickers[token_information["underlying"]]["last"]
                else:
                    underlying_price = None

                result.append({
                    "name": token_information["name"],
                    "description": token_information["description"],
                    "underlying": token_information["underlying"],
                    "underlyingPrice": underlying_price,
                    "leverage": token_information["leverage"],
                    "outstanding": token_information["outstanding"],
                    "pricePerShare": token_information["pricePerShare"],
                    "positionPerShare": token_information["positionPerShare"],
                    "positionsPerShare": token_information["positionsPerShare"],
                    "basketUnderlying": token_information["basket"][token_information["underlying"]],
                    "basketUSD": token_information["basket"]["USD"],
                    "targetComponents": token_information["targetComponents"],
                    "underlyingMark": token_information["underlyingMark"],
                    "totalNav": token_information["totalNav"],
                    "totalCollateral": token_information["totalCollateral"],
                    "currentLeverage": token_information["currentLeverage"],
                    "change1h": token_information["change1h"],
                    "change24h": token_information["change24h"],
                    "changeBod": token_information["changeBod"]
                })

            if update_db:
                mariadb_interface.db_insert_raw(
                    "rebalancing_raw", "ftx", datetime.utcnow().replace(tzinfo=pytz.UTC), "get_etf_token_informations", str(result))

            return result
        except Exception:
            logger.error("Exception while fetching ETF token information")
            traceback.print_exc()

    return None


def get_price_for_symbol_from_ticker(symbol, tickers, update_db=True):
    """
    Get the price for a symbol from a tickers object and store it in the db in the "rebalancing_price" table.
    :param symbol: Symbol to use
    :param tickers: Tickers where the price shall be retrieved from
    :param update_db: True if db shall be updated, False if not.
    :return current price in USD
    """
    latest_price = None
    try:
        logger.debug("Finding ticker price for " + symbol)

        latest_price = tickers[symbol]['last']

        if update_db:
            metadata = '{ ' + \
                '"symbol": "' + symbol + '"' + \
                ' }'
            mariadb_interface.db_insert_raw(
                table='rebalancing_raw', exchange='ftx', timestamp=datetime.utcnow(), information_type="price", metadata=metadata, value=str(latest_price))
    except Exception as e:
        logger.critical(e)
        logger.critical(traceback.format_exc())

    return latest_price


def get_trades(symbol, since=None):
    """
    Get the last trades for a symbol from the exchange and store it in the db in the "rebalancing_trades" table.
    :param symbol: Symbol to use
    :param since Timestamp that defines since when the trades shall be retrieved. Default: None.
    :return trades for the symbol
    """
    trades = None
    try:
        logger.debug("Fetching last trades for symbol " +
                     symbol + (" since " + str(since) if since else ""))
        trades = ftx.fetch_trades(symbol=symbol)
        mariadb_interface.db_insert_trades(table='rebalancing_trades', exchange='ftx',
                                           symbol=symbol, timestamp=datetime.utcnow(), since=since, limit_num=None, trades=trades)
    except Exception as e:
        logger.critical(e)
        logger.critical(traceback.format_exc())

    return trades


def get_order_book(symbol, update_db=False, max_sim_time_increase=timedelta(seconds=5), depth=None, grouping=None):
    """
    Get the order book for a symbol from the exchange and store it in the db in the "rebalancing_raw" table (optional).
    :param symbol: Symbol to use
    :param update_db: Add entry to db or not.
    :param max_sim_time_increase: (Simulation mode only) Max timedelta from now where data shall be searched for (5 seconds default). If no data was found in this timedelta, None is returned.
    :return order book for the symbol
    """
    if not depth:
        raise Exception("Depth must be specified")

    if not grouping:
        raise Exception("Grouping must be specified")

    result = None
    logger.debug("Fetching order book for symbol " +
                 symbol + " (depth: " + str(depth) + ", grouping: " + str(grouping) + ")")

    if mode_selector.simulation_mode:
        result = get_next_order_book(
            symbol=symbol, max_sim_time_increase=max_sim_time_increase)
    else:
        try:
            r = requests.get("https://ftx.com/api/markets/BTC-PERP/orderbook_grouped?depth=" +
                             str(depth) + "&grouping=" + str(grouping), timeout=15)

            r.raise_for_status()  # raise Exception in case request didn't succeed with HTTP 200

            # use orjson here because it can properly print json objects/arrays to a string (in contrast to cysimdjson) and it's still really fast
            result = orjson.loads(r.text)
            result = result["result"]

            if update_db:
                metadata = '{ ' + \
                    '"symbol": "' + symbol + '",' + \
                    ' }'
                mariadb_interface.db_insert_raw(table="rebalancing_raw",
                                                exchange="ftx", timestamp=datetime.utcnow(), information_type="order_book", metadata=metadata, value=str(result))
        except Exception as e:
            logger.critical(e)
            logger.critical(traceback.format_exc())

    try:
        if result:
            result = dict(result)
            result["depth"] = depth
            result["grouping"] = grouping
            result["bids"] = list([[value for value in elem]
                                   for elem in result["bids"]])
            result["asks"] = list([[value for value in elem]
                                   for elem in result["asks"]])
    except Exception as e:
        logger.critical(e)
        logger.critical(traceback.format_exc())
    return result


def get_order_books(update_db=False, max_sim_time_increase=timedelta(seconds=5), table=None, symbols=[], depth=None, grouping=None):
    """
    Get the order books for given symbols and store it in the db (optional).
    :param update_db: Add entry to db or not.
    :param max_sim_time_increase: (Simulation mode only) Max timedelta from now where data shall be searched for (5 seconds default). If no data was found in this timedelta, None is returned.
    :return order books
    """

    if not depth:
        raise Exception("Depth must be specified")

    if not grouping:
        raise Exception("Grouping must be specified")

    result = None
    logger.debug("Fetching order books (depth: " + str(depth) +
                 ", grouping: " + str(grouping) + ")")
    if mode_selector.simulation_mode:
        # first try to get entry where informationType=="order_books" (all in one row)
        result = get_next_order_books(
            max_sim_time_increase=max_sim_time_increase)

        # if this didn't succeed, aggregate all order books from single rows
        if not result:
            logger.debug(
                "No aggregated \"order_books\" found, aggregating them manually...")
            result = {
                symbol: get_order_book(symbol=symbol,
                                       max_sim_time_increase=max_sim_time_increase,
                                       update_db=False)
                for symbol in rebalancing_symbols.all_symbols if symbol.endswith("-PERP")
            }

            # remove None (if no order book could be found for the token)
            result = {k: v for k,
                      v in result.items() if v is not None}
    else:
        order_book_list = {}
        with ThreadPoolExecutor(len(symbols)) as e:
            futures = [e.submit(get_order_book, symbol, False, None, depth, grouping)
                       for symbol in symbols]
            # Optional: workers terminate as soon as all futures finish,
            e.shutdown(False)
            # rather than waiting for all results to be processed

            for index, fut in enumerate(futures):
                try:
                    order_book_list[symbols[index]] = fut.result()
                except Exception as e:
                    logger.error("Exception occured in thread: " + str(e))

        result = {
            symbol: {
                "bids": order_book["bids"], "asks": order_book["asks"]}
            for symbol, order_book in order_book_list.items() if order_book
        }

        # remove None (if no order book could be found for the token)
        result = {k: v for k,
                  v in result.items() if v is not None}

        if update_db:
            mariadb_interface.db_insert_raw(table=table,
                                            exchange="ftx", timestamp=datetime.utcnow(), information_type="order_books", metadata="{\"depth\": " + str(depth) + ", \"grouping\": " + str(grouping) + "}", value=str(result))

    return result


def get_price_ohlcv_single(when, timeframe, symbol, result, lock):
    """
    Get the ohlcv data (open, high, low, close, volume) for a symbol from the exchange and at a given time.
    :param when: Timestamp of the ohlcv data requested
    :param timeframe defines the timeframe of the ohlcv
    :param symbol Symbole for which the ohlcv data is requested
    :param result List where result shall be appended to. A new entry is is appended which has this format: (symbol, [open, high, low, close, volume]).
    :param lock threading.Lock used secure access to the result parameter when appending new results
    :return list of (symbol, [open, high, low, close, volume])
    """
    try:
        logger.debug("Fetching ohlcv for symbol " +
                     symbol + " and timeframe " + timeframe + " at " + str(when))

        ohlcv = ftx.fetch_ohlcv(
            symbol=symbol, timeframe=timeframe, since=when.timestamp() * 1000, limit=1
        )

        lock.acquire()
        result.append(
            (symbol, [ohlcv[0][1], ohlcv[0][2], ohlcv[0][3], ohlcv[0][4], ohlcv[0][5]]))
        lock.release()
    except ccxt.RequestTimeout:
        logger.error(
            "Timeout while fetching ohlcv data for " + symbol)
    except Exception as e:
        logger.error(
            "Exception while fetching rebalancings for symbol " + symbol + ": " + str(type(e).__name__) + ", " + str(e))
        traceback.print_exc()


def get_prices_ohlcv(when, timeframe, symbols):
    """
    Get the ohlcv data (open, high, low, close, volume) for multiple symbols from the exchange and at a given time.
    :param when: Timestamp of the ohlcv data requested
    :param timeframe defines the timeframe of the ohlcv
    :param symbols list of symboles for which the ohlcv data is requested
    :return list of (symbol, [open, high, low, close, volume])
    """
    result = []

    with ThreadPoolExecutor(len(symbols)) as e:
        lock = threading.Lock()
        for symbol in symbols:
            e.submit(get_price_ohlcv_single, when, timeframe, symbol, result, lock).add_done_callback(
                thread_exception_handler.thread_exception_check_callback)

    return result


def get_ohlcv(symbol, timeframe, update_db=False, since=None, table=None):
    """
    Get the ohlcv data (open, high, low, close, volume) for a symbol from the exchange and store it in the db in the "rebalancing_ohlcv" table.
    If the timeframe parameter is '15s', the ohlcv of the previous 15 seconds are retrieved (because the current 15 seconds don't yet have a close price yet).
    If the timeframe parameter is '1m', the ohlcv of the previous minute is retrieved (because the current minute doesn't yet have a close price yet).
    If the timeframe parameter is '5m', the ohlcv of the previous 5 minutes is retrieved (because the current 5 minutes don't yet have a close price yet).
    :param symbol: Symbol to use
    :param timeframe defines the timeframe of the ohlcv
    :param update_db: True if db shall be updated, False if not.
    :param since get ohlcv data since this datetime object (UTC) - must have tzinfo=pytz.UTC. If None, the current time is used (hence, only one ohlcv entry will be returned).
    :return datetime object representing start of ohlcv data, ohlcv for the symbol
    """
    if not symbol:
        raise Exception("Symbol must be specified")

    if mode_selector.simulation_mode:
        raise Exception("not implemented")

    ohlcvs = None
    try:
        logger.debug("Fetching current ohlcv for symbol " +
                     symbol + " and timeframe " + timeframe)

        if not since:
            dt = datetime.utcnow()

            # make datetime object aware of the UTC timezone
            dt = dt.replace(tzinfo=pytz.UTC)

            if timeframe == '15s':
                dt -= timedelta(seconds=15)  # last 15 seconds
                dt = dt.replace(microsecond=0)
            elif timeframe == '1m':
                dt -= timedelta(minutes=1)  # last minute
                dt = dt.replace(second=0, microsecond=0)
            elif timeframe == '5m':
                dt -= timedelta(minutes=5)  # 5 minutes before
                dt = dt.replace(second=0, microsecond=0)
            else:
                raise Exception('Unsupported timeframe')
        else:
            dt = since

        limit = (None if since else 1)

        ohlcvs = ftx.fetch_ohlcv(
            symbol=symbol, timeframe=timeframe, since=dt.timestamp() * 1000, limit=limit
        )

        if update_db:
            metadata = '{ ' + \
                '"symbol": "' + symbol + '", ' + \
                '"timeframe": "' + timeframe + '", ' + \
                '"since": ' + str(dt.timestamp()) + ', ' + \
                '"limit":' + (str(limit) if limit else str(0)) + \
                ' }'
            mariadb_interface.db_insert_raw(
                table=table, exchange="ftx", timestamp=datetime.utcnow(), information_type="ohlcvs", metadata=metadata, value=str(ohlcvs))
    except Exception as e:
        logger.critical(e)
        logger.critical(traceback.format_exc())

    return dt, ohlcvs


def get_ohlcvs(timeframe, symbols=[], update_db=False, since=None, table=None):
    """
    Get the ohlcv data (open, high, low, close, volume) for multiple symbols from the exchange and store it in the db in the "rebalancing_ohlcv" table.
    If the timeframe parameter is '15s', the ohlcv of the previous 15 seconds are retrieved (because the current 15 seconds don't yet have a close price yet).
    If the timeframe parameter is '1m', the ohlcv of the previous minute is retrieved (because the current minute doesn't yet have a close price yet).
    If the timeframe parameter is '5m', the ohlcv of the previous 5 minutes is retrieved (because the current 5 minutes don't yet have a close price yet).
    :param symbol: Symbol to use
    :param timeframe defines the timeframe of the ohlcv
    :param update_db: True if db shall be updated, False if not.
    :param since get ohlcv data since this datetime object (UTC) - must have tzinfo=pytz.UTC. If None, the current time is used (hence, only one ohlcv entry will be returned).
    :return datetime object representing start of ohlcv data, ohlcv for the symbol
    """
    if not symbols or len(symbols) == 0:
        raise Exception("Symbols must be specified")

    if mode_selector.simulation_mode:
        raise Exception("not implemented")

    result = None
    try:
        logger.debug("Fetching ohlcv for symbols " +
                     str(symbols) + " and timeframe " + timeframe + (" since " + str(since) if since else ""))

        if not since:
            dt = datetime.utcnow()

            # make datetime object aware of the UTC timezone
            dt = dt.replace(tzinfo=pytz.UTC)

            if timeframe == '15s':
                dt -= timedelta(seconds=15)  # last 15 seconds
                dt = dt.replace(microsecond=0)
            elif timeframe == '1m':
                dt -= timedelta(minutes=1)  # last minute
                dt = dt.replace(second=0, microsecond=0)
            elif timeframe == '5m':
                dt -= timedelta(minutes=dt.minute % 5)
                dt -= timedelta(minutes=5)  # 5 minutes before
                dt = dt.replace(second=0, microsecond=0)
            else:
                raise Exception('Unsupported timeframe')
        else:
            dt = since

        limit = (None if since else 1)

        ohlcv_list = []
        with ThreadPoolExecutor(len(symbols)) as e:
            futures = [e.submit(get_ohlcv, symbol, timeframe, False, None, table)
                       for symbol in symbols]
            # Optional: workers terminate as soon as all futures finish,
            e.shutdown(False)
            # rather than waiting for all results to be processed

            for fut in futures:
                try:
                    ohlcv_list.append(fut.result())
                except Exception as e:
                    logger.error("Exception occured in thread: " + str(e))

        result = {
            symbols[index]: {
                "open": ohlcv[1][0][1],
                "high": ohlcv[1][0][2],
                "low": ohlcv[1][0][3],
                "close": ohlcv[1][0][4],
                "volume": ohlcv[1][0][5]}
            for index, ohlcv in enumerate(ohlcv_list) if ohlcv and len(ohlcv) > 0 and len(ohlcv[1]) > 0 and len(ohlcv[1][0]) > 0
        }

        # remove None (if no order book could be found for the token)
        result = {k: v for k,
                  v in result.items() if v is not None}

        if update_db:
            metadata = '{ ' + \
                '"timeframe": "' + timeframe + '", ' + \
                '"since": ' + str(int(dt.timestamp())) + ', ' + \
                '"limit":' + (str(limit) if limit else str(0)) + \
                ' }'
            mariadb_interface.db_insert_raw(
                table=table, exchange="ftx", timestamp=dt, information_type="ohlcvs", metadata=metadata, value=str(result))
    except Exception as e:
        logger.critical(e)
        logger.critical(traceback.format_exc())

    return result


def get_price_at_last_rebalancing(symbol, timeframe):
    """
    Get the price at the last rebalancing for a symbol from the exchange.
    :param symbol: Symbol to use
    :param timeframe: time frame for which the price ohlcv shall be retrieved. Currently, only '5m' supported.
    :return price of the symbol at the last rebalancing event
    """
    ohlcvs = None
    try:
        logger.debug("Fetching price at last rebalancing for symbol " +
                     symbol + " and timeframe " + timeframe)

        dt = datetime.utcnow()

        # make datetime object aware of the UTC timezone
        dt = dt.replace(tzinfo=pytz.UTC)

        if timeframe == '5m':
            dt -= timedelta(days=1)  # last day
            dt = dt.replace(hour=0, minute=0, second=0,
                            microsecond=0)  # set 0:00
        else:
            raise Exception("Only 5m timeframe supported")

        # Use a timeframe here which involves the opening and closing price of the rebalancing event!
        ohlcvs = ftx.fetch_ohlcv(
            symbol=symbol, timeframe=timeframe, since=dt.timestamp() * 1000, limit=1
        )
    except Exception as e:
        logger.critical(e)
        logger.critical(traceback.format_exc())

    return [dt, ohlcvs]


# TODO: implement
def market_close_all_positions():
    try:
        logger.warning("Market closing all positions!")
        logger.critical("Market close not implemented!")
    except Exception as e:
        logger.critical(e)
        logger.critical(traceback.format_exc())


def open_position(order: Order):
    if order.order_type == OrderType.LONG:
        if mode_selector.simulation_mode:
            order.open_price = get_tickers(
                max_sim_time_increase=None)[order.symbol]["ask"]
        else:
            raise Exception("Not implemented!")

        logger.info("Opening " + str(order.leverage) + "x LONG position for " +
                    order.symbol + " at $" + str(order.open_price))
    elif order.order_type == OrderType.SHORT:
        if mode_selector.simulation_mode:
            order.open_price = get_tickers(
                max_sim_time_increase=None)[order.symbol]["bid"]
        else:
            raise Exception("Not implemented!")

        logger.info("Opening " + str(order.leverage) + "x SHORT position for " +
                    order.symbol + " at $" + str(order.open_price))
    else:
        raise Exception("Unknown order type: " + str(order.order_type))


def close_position(order: Order):
    if order.order_type == OrderType.LONG:
        if mode_selector.simulation_mode:
            order.close_price = get_tickers(update_db=False,
                                            max_sim_time_increase=None)[order.symbol]["bid"]

            total_fees = takerFee*order.leverage*simulation.balance
            simulation.balance = simulation.balance * \
                (1+(order.close_price/order.open_price-1.0)*order.leverage)-total_fees
        else:
            raise Exception("Not implemented!")

        logger.info("Closing " + str(order.leverage) + "x LONG position for " +
                    order.symbol + " at $" + str(order.close_price))
    elif order.order_type == OrderType.SHORT:
        if mode_selector.simulation_mode:
            order.close_price = get_tickers(
                max_sim_time_increase=None)[order.symbol]["ask"]

            total_fees = takerFee*order.leverage*simulation.balance
            simulation.balance = simulation.balance * \
                (1-(order.close_price/order.open_price-1.0)*order.leverage)-total_fees
        else:
            raise Exception("Not implemented!")

        logger.info("Closing " + str(order.leverage) + "x SHORT position for " +
                    order.symbol + " at $" + str(order.close_price))
    else:
        raise Exception("Unknown order type: " + str(order.order_type))
