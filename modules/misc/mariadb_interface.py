import mariadb
from datetime import datetime
import pytz
import threading

from modules.misc.logger import logger
from modules.misc import mode_selector as mode_selector
from config.MariadbConfig import MariadbConfig

global db
db = None

lock = threading.Lock()


def initialize_db():
    """
    Initializes a new db "autocrypto" with all tables. NOT thread-safe.
    """
    logger.info("Initializing new database")
    cursor = db.cursor()

    cursor.execute("CREATE DATABASE IF NOT EXISTS autocrypto;")

    # select database
    cursor.execute("USE autocrypto;")

    # create table 'balance'
    cursor.execute("CREATE TABLE balance("
                   "id INT NOT NULL AUTO_INCREMENT, "
                   "exchange VARCHAR(30) NOT NULL, "
                   "symbol VARCHAR(30) NOT NULL, "  # "total" possible value
                   "timestamp DATETIME NOT NULL, "
                   "value FLOAT NOT NULL, "  # usd
                   "PRIMARY KEY(id)"
                   ");"
                   )

    # # create table 'rebalancing_orderbook'
    # cursor.execute("CREATE TABLE rebalancing_orderbook("
    #                "id INT NOT NULL AUTO_INCREMENT, "
    #                "exchange VARCHAR(30) NOT NULL, "
    #                "symbol VARCHAR(30) NOT NULL, "
    #                "timestamp DATETIME NOT NULL, "
    #                "type VARCHAR(10) NOT NULL, "  # "bids"/"asks"
    #                "content TEXT NOT NULL, "
    #                "PRIMARY KEY(id)"
    #                ");"
    #                )

    # # create table 'rebalancing_ohlcv'
    # cursor.execute("CREATE TABLE rebalancing_ohlcv("
    #                "id INT NOT NULL AUTO_INCREMENT, "
    #                "exchange VARCHAR(30) NOT NULL, "
    #                "symbol VARCHAR(30) NOT NULL, "
    #                "timeframe VARCHAR(5) NOT NULL, "
    #                "timestamp DATETIME NOT NULL, "
    #                "open FLOAT NOT NULL, "
    #                "high FLOAT NOT NULL, "
    #                "low FLOAT NOT NULL, "
    #                "close FLOAT NOT NULL, "
    #                "volume FLOAT NOT NULL, "
    #                "PRIMARY KEY(id)"
    #                ");"
    #                )

    # # create table 'rebalancing_price'
    # cursor.execute("CREATE TABLE rebalancing_price("
    #                "id INT NOT NULL AUTO_INCREMENT, "
    #                "exchange VARCHAR(30) NOT NULL, "
    #                "symbol VARCHAR(30) NOT NULL, "
    #                "timestamp DATETIME NOT NULL, "
    #                "value FLOAT NOT NULL, "  # usd
    #                "PRIMARY KEY(id)"
    #                ");"
    #                )

    # # create table 'rebalancing_trades'
    # cursor.execute("CREATE TABLE rebalancing_trades("
    #                "id INT NOT NULL AUTO_INCREMENT, "
    #                "exchange VARCHAR(30) NOT NULL, "
    #                "symbol VARCHAR(30) NOT NULL, "
    #                "timestamp DATETIME NOT NULL, "
    #                "since DATETIME, "
    #                "limit_num INT, "
    #                "trades TEXT NOT NULL, "  # usd
    #                "PRIMARY KEY(id)"
    #                ");"
    #                )

    # # create table 'rebalancing_symbol_information'
    # cursor.execute("CREATE TABLE rebalancing_symbol_information("
    #                "id INT NOT NULL AUTO_INCREMENT, "
    #                "exchange VARCHAR(30) NOT NULL, "
    #                "timestamp DATETIME NOT NULL, "
    #                "name VARCHAR(30) NOT NULL, "
    #                "description TEXT, "
    #                "underlying VARCHAR(30) NOT NULL, "
    #                "underlyingPrice FLOAT, "
    #                "currentLeverage FLOAT NOT NULL, "
    #                "leverage FLOAT NOT NULL, "
    #                "outstanding FLOAT NOT NULL, "
    #                "pricePerShare FLOAT NOT NULL, "
    #                "positionPerShare FLOAT NOT NULL, "
    #                #"positionsPerShare FLOAT NOT NULL, "
    #                "basketUnderlying FLOAT NOT NULL, "
    #                "basketUSD FLOAT NOT NULL, "
    #                #"targetComponents FLOAT NOT NULL, "
    #                "underlyingMark FLOAT NOT NULL, "
    #                "totalNav FLOAT NOT NULL, "
    #                "totalCollateral FLOAT NOT NULL, "
    #                "PRIMARY KEY(id)"
    #                ");"
    #                )

    # create table 'rebalancing_raw'
    cursor.execute("CREATE TABLE rebalancing_raw("
                   "id INT NOT NULL AUTO_INCREMENT, "
                   "exchange VARCHAR(30) NOT NULL, "
                   "timestamp DATETIME NOT NULL, "
                   "metadata TEXT, "
                   "informationType VARCHAR(30) NOT NULL, "
                   "value LONGTEXT NOT NULL, "
                   "PRIMARY KEY(id)"
                   ");"
                   )

    # create table 'recording'
    cursor.execute("CREATE TABLE recording("
                   "id INT NOT NULL AUTO_INCREMENT, "
                   "exchange VARCHAR(30) NOT NULL, "
                   "timestamp DATETIME NOT NULL, "
                   "metadata TEXT, "
                   "informationType VARCHAR(30) NOT NULL, "
                   "value LONGTEXT NOT NULL, "
                   "PRIMARY KEY(id)"
                   ");"
                   )

    cursor.close()


def connect_db():
    """
    Connects to an existing db "autocrypto". If it doesn't exist, a new one is created. Thread-safe.
    """
    logger.info("Connecting to db")
    lock.acquire()
    try:
        global db
        if not db:
            config = MariadbConfig()
            db = mariadb.connect(
                user=config.user,
                password=config.password,
                host=config.host
            )

            cursor = db.cursor()

            # if database doesn't exist, create it with all tables
            cursor.execute("SHOW DATABASES LIKE 'autocrypto';")
            if not cursor.fetchone():
                initialize_db()
            else:
                # database has already been created
                cursor.execute("USE autocrypto;")

            cursor.close()
    finally:
        lock.release()


def disconnect_db():
    """
    Disconnects from the db if it's connected. Thread-safe.
    """
    logger.info("Disconnecting from db")
    lock.acquire()
    try:
        global db
        if db:
            db.close()
            db = None
    finally:
        lock.release()


def db_insert_balance(table, exchange, symbol, timestamp, value):
    """
    Inserts a balance entry to a given table in the db. Thread-safe.
    :param table Table name
    :param exchange Exchange name
    :param symbol Symbol
    :param timestamp Timestamp of the balance entry
    :param value Value in USD
    """
    if mode_selector.test_mode:
        return
    elif mode_selector.simulation_mode:
        return

    lock.acquire()
    try:
        cursor = db.cursor()
        cursor.execute("INSERT INTO " + table +
                       " (exchange, symbol, timestamp, value) VALUES (?, ?, ?, ?);", (exchange, symbol, timestamp.strftime('%Y-%m-%d %H:%M:%S'), value))
        db.commit()
        cursor.close()
    finally:
        cursor.close()
        lock.release()


def db_insert_order_book(table, exchange, timestamp, order_book):
    """
    Inserts a new order book entry to a given table in the db. Asks and bids create separate, dedicated entries in the db table.  Thread-safe.
    :param table Table name
    :param exchange Exchange name
    :param timestamp Timestamp of the balance entry
    :param order_book The order book (includes the symbol name)
    """
    if mode_selector.test_mode:
        return
    elif mode_selector.simulation_mode:
        return

    lock.acquire()
    try:
        cursor = db.cursor()
        if order_book['bids'] and order_book['bids'] != "" and order_book['bids'] != "[]":
            cursor.execute("INSERT INTO " + table +
                           " (exchange, symbol, timestamp, type, content) VALUES (?, ?, ?, ?, ?);", (exchange, order_book['symbol'], timestamp.strftime('%Y-%m-%d %H:%M:%S'), 'bids', str(order_book['bids'])))
        if order_book['asks'] and order_book['asks'] != "" and order_book['asks'] != "[]":
            cursor.execute("INSERT INTO " + table +
                           " (exchange, symbol, timestamp, type, content) VALUES (?, ?, ?, ?, ?);", (exchange, order_book['symbol'], timestamp.strftime('%Y-%m-%d %H:%M:%S'), 'asks', str(order_book['asks'])))
        db.commit()
    finally:
        cursor.close()
        lock.release()


def db_insert_ohlcv(table, exchange, symbol, timeframe, ohlcv):
    """
    Inserts a ohlcv entry to a given table in the db. Thread-safe.
    :param table Table name
    :param exchange Exchange name
    :param symbol Symbol
    :param timeframe timeframe of the ohlcv entry (e.g. "5m")
    :param ohlcv OHLCV entry (must contain the timestamp of the OHLCV beginning). The ohlcv parameter must only contain one element (more are not supported currently).
    """
    if mode_selector.test_mode:
        return
    elif mode_selector.simulation_mode:
        return

    lock.acquire()
    try:
        cursor = db.cursor()
        if len(ohlcv) != 1:
            logger.error("Inserting multiple ohlcv tuples not implemented")
        else:
            cursor.execute("INSERT INTO " + table +
                           " (exchange, symbol, timeframe, timestamp, open, high, low, close, volume) VALUES "
                           "(?, ?, ?, ?, ?, ?, ?, ?, ?);", (exchange, symbol, timeframe, datetime.fromtimestamp(ohlcv[0][0]/1000).astimezone(pytz.utc).strftime('%Y-%m-%d %H:%M:%S'), ohlcv[0][1], ohlcv[0][2], ohlcv[0][3], ohlcv[0][4], ohlcv[0][5]))
            db.commit()
    finally:
        cursor.close()
        lock.release()


def db_insert_price(table, exchange, symbol, timestamp, value):
    """
    Inserts a price entry to a given table in the db. Thread-safe.
    :param table Table name
    :param exchange Exchange name
    :param symbol Symbol
    :param timestamp Timestamp of the price
    :param value Price in USD of the symbol at the given timestamp
    """
    if mode_selector.test_mode:
        return
    elif mode_selector.simulation_mode:
        return

    lock.acquire()
    try:
        cursor = db.cursor()
        cursor.execute("INSERT INTO " + table +
                       " (exchange, symbol, timestamp, value) VALUES (?, ?, ?, ?);", (exchange, symbol, timestamp.strftime('%Y-%m-%d %H:%M:%S'), value))
        db.commit()
    finally:
        cursor.close()
        lock.release()


def db_insert_trades(table, exchange, symbol, timestamp, trades, since=None, limit_num=None):
    """
    Inserts a trades entry entry to a given table in the db. (One new entry holding all trades with all trades as a string.) Thread-safe.
    :param table Table name
    :param exchange Exchange name
    :param symbol Symbol
    :param timestamp Timestamp of the price
    :param trades Trades to put into the db table. Exactly one new entry will be created that holds all trades.
    :param since Timestamp of then the collection of trades started. Can be None.
    :param limit_num Limit number for the trades. Can be None.
    """
    if mode_selector.test_mode:
        return
    elif mode_selector.simulation_mode:
        return

    lock.acquire()
    try:
        cursor = db.cursor()
        if len(trades) == 0:
            logger.error("Trades are empty")
        else:
            cursor.execute("INSERT INTO " + table +
                           " (exchange, symbol, timestamp, since, limit_num, trades) VALUES "
                           "(?, ?, ?, ?, ?, ?);", (exchange, symbol, timestamp.strftime('%Y-%m-%d %H:%M:%S'), (since.strftime('%Y-%m-%d %H:%M:%S') if since else ""), (limit_num if limit_num else 0), str(trades)))
            db.commit()
    finally:
        cursor.close()
        lock.release()


def db_insert_symbol_information(table, exchange, timestamp, symbol_information):
    """
    Inserts a symbol information entry entry to a given table in the db. Thread-safe.
    :param table Table name
    :param exchange Exchange name
    :param timestamp Timestamp (datetime)
    :param symbol_information Token information dict
    """
    if mode_selector.test_mode:
        return
    elif mode_selector.simulation_mode:
        return

    lock.acquire()
    try:
        cursor = db.cursor()
        cursor.execute("INSERT INTO " + table +
                       " (exchange, timestamp, name, description, underlying, underlyingPrice, currentLeverage, leverage, outstanding, pricePerShare, positionPerShare, basketUnderlying, basketUSD, underlyingMark, totalNav, totalCollateral) VALUES "
                       "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", (exchange, timestamp, symbol_information["name"], symbol_information["description"], symbol_information["underlying"], symbol_information["underlyingPrice"], symbol_information["currentLeverage"], symbol_information["leverage"], symbol_information["outstanding"], symbol_information["pricePerShare"], symbol_information["positionPerShare"], symbol_information["basketUnderlying"], symbol_information["basketUSD"], symbol_information["underlyingMark"], symbol_information["totalNav"], symbol_information["totalCollateral"]))
        db.commit()
    finally:
        cursor.close()
        lock.release()


def db_insert_raw(table, exchange, timestamp, information_type, value, metadata=None):
    """
    Inserts raw string data to a given table in the db. Thread-safe.
    :param table Table name
    :param exchange Exchange name
    :param timestamp Timestamp (datetime)
    :param information_type Type of the information
    :param value Value to store
    """
    if mode_selector.test_mode:
        return
    elif mode_selector.simulation_mode:
        return

    if not table:
        logger.error("Table parameter is None. Could not insert data into db.")
        return

    if not exchange:
        logger.error(
            "Exchange parameter is None. Could not insert data into db.")
        return

    if not timestamp:
        logger.error(
            "Timestamp parameter is None. Could not insert data into db.")
        return

    if not information_type:
        logger.error(
            "Information type parameter is None. Could not insert data into db.")
        return

    if not value:
        logger.error("Value parameter is None. Could not insert data into db.")
        return

    lock.acquire()
    try:
        cursor = db.cursor()
        cursor.execute("INSERT INTO " + table +
                       " (exchange, timestamp, informationType, metadata, value) VALUES "
                       "(?, ?, ?, ?, ?);",
                       (
                           exchange,
                           timestamp,
                           information_type,
                           (metadata if metadata else ""),
                           value.replace("'", "\"")
                                .replace(": None", ": null")
                                .replace(": True", ": true")
                                .replace(": False", ": false")
                       )
                       )
        db.commit()
    finally:
        cursor.close()
        lock.release()


def db_get_prices_for_day(table, exchange, symbol, timestamp):
    lock.acquire()
    result = None
    try:
        cursor = db.cursor()
        cursor.execute("SELECT * FROM " + table +
                       " WHERE exchange = '" + exchange + "' AND symbol = '" + symbol + "'" +
                       " AND timestamp BETWEEN '" + timestamp.strftime('%Y-%m-%d') + " 00:00:00' AND '" + timestamp.strftime('%Y-%m-%d') + " 23:59:59'" +
                       " GROUP BY timestamp ASC;")
        result = cursor.fetchall()
    finally:
        cursor.close()
        lock.release()

    return result


def db_get_ohlcv_for_day(table, exchange, symbol, timestamp, timeframe):
    lock.acquire()
    result = None
    try:
        cursor = db.cursor()
        cursor.execute("SELECT * FROM " + table +
                       " WHERE exchange = '" + exchange + "' AND symbol = '" +
                       symbol + "' AND timeframe = '" + timeframe + "'"
                       " AND timestamp BETWEEN '" + timestamp.strftime('%Y-%m-%d') + " 00:00:00' AND '" + timestamp.strftime('%Y-%m-%d') + " 23:59:59'" +
                       " GROUP BY timestamp ASC;")
        result = cursor.fetchall()
    finally:
        cursor.close()
        lock.release()

    return result


def db_get_all_symbols_from_table(table, exchange, day=None):
    lock.acquire()
    result = None
    try:
        cursor = db.cursor()
        cursor.execute("SELECT symbol FROM " + table +
                       " WHERE exchange = '" + exchange +
                       ((" AND timestamp BETWEEN '" + day.strftime('%Y-%m-%d') + " 00:00:00' AND '" + day.strftime('%Y-%m-%d') + " 23:59:59'") if day else "") +
                       "';")
        result = cursor.fetchall()
    finally:
        cursor.close()
        lock.release()

    result = list(set([symbol[0] for symbol in result]))

    return result


def db_get_data_from_table(table, exchange, day=None):
    lock.acquire()
    result = None
    try:
        cursor = db.cursor()
        cursor.execute("SELECT * FROM " + table + " WHERE exchange = '" + exchange + "'" +
                       ((" AND timestamp BETWEEN '" + day.strftime('%Y-%m-%d') + " 00:00:00' AND '" + day.strftime('%Y-%m-%d') + " 23:59:59'") if day else "") +
                       ";")
        result = cursor.fetchall()
    finally:
        cursor.close()
        lock.release()

    return result
