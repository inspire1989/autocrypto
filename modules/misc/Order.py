from enum import Enum


class OrderType(Enum):
    LONG = 0
    SHORT = 1


class Order:
    def __init__(self, symbol: str, order_type: OrderType, leverage: float) -> None:
        self.symbol = symbol
        self.order_type = order_type
        self.leverage = leverage

        assert self.leverage > 0, "Leverage must be >0!"

        self.open_price = None
        self.close_price = None
