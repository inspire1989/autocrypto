from modules.misc.logger import logger

# Callback to be set by user code. This gets executed when an exception within a thread was captured.
global exception_callback
exception_callback = None


def thread_exception_check_callback(f):
    """
    Used to capture and show exceptions from worker threads of the ThreadPoolExecutor.
    """
    e = f.exception()

    if e is None:
        return

    trace = []
    tb = e.__traceback__
    while tb is not None:
        trace.append({
            "filename": tb.tb_frame.f_code.co_filename,
            "name": tb.tb_frame.f_code.co_name,
            "lineno": tb.tb_lineno
        })
        tb = tb.tb_next

    if e.__class__.__name__ == KeyboardInterrupt:
        logger.warning(
            "KeyboardInterrupt captured. Open positions on exchange haven't been closed!")
    else:
        logger.critical("Exception in thread")
        logger.critical("Type: " + str(type(e).__name__))
        logger.critical("Message: " + str(e))
        logger.critical("Trace: ")
        for t in trace:
            logger.critical("\t File \"" + str(t["filename"]) + "\", line " + str(t["lineno"]) + ", function \"" + str(
                t["name"]) + "\"")

        exception_callback()
