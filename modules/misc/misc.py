from modules.misc.Exceptions import TooLateException
import time
import modules.simulation.simulation as simulation
from modules.misc.logger import logger
import modules.misc.mode_selector as mode_selector
from datetime import datetime, timedelta
import pause
import pytz
import os


def get_root_folder():
    return os.path.abspath(os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + '/..'))


def select_file_from_folder(folder):
    files = [f for f in os.listdir(
        folder) if os.path.isfile(os.path.join(folder, f))]

    logger.info("History files found:")
    for index, file in enumerate(files):
        logger.info(str(index+1) + ": " + file)

    success = False

    while not success:
        try:
            number = int(input("Enter a number: "))

            if number <= 0:
                logger.error("File number must be greater than 0.")
                continue
            if number > len(files):
                logger.error("File number out of range.")
                continue

            success = True
        except ValueError:
            success = False

    return files[number-1]


def verify_time_is_before(timestamp):
    """
    Checks if the current time is smaller or equals a given timestamp. 
    Raises a TooLateException if the current time is after the given timestmap.
    Works for real-time and simulation.
    :param timestamp Timestamp to compare to. If "now" is > this timestamp, a TooLateException is raised.
    """
    if mode_selector.simulation_mode:
        if simulation.sim_time:
            if simulation.sim_time >= timestamp:
                raise TooLateException(
                    now=simulation.sim_time, verify_timestamp=timestamp)
        else:
            if simulation.sim_day >= timestamp:
                raise TooLateException(
                    now=simulation.sim_day, verify_timestamp=timestamp)
    else:
        now = datetime.utcnow().replace(tzinfo=pytz.UTC)
        if now > timestamp:
            raise TooLateException(now=now, verify_timestamp=timestamp)


def sleep_for(seconds):
    """
    Sleeps for some seconds. Works for real-time and for simulation.
    :param seconds Amount of seconds to sleep
    """
    if mode_selector.simulation_mode:
        logger.debug("Simulating sleep of " + str(seconds) + " seconds")

        if simulation.sim_time:
            simulation.skip_sim_time(
                simulation.sim_time+timedelta(seconds=seconds))
        else:
            simulation.skip_sim_time(
                simulation.sim_day+timedelta(seconds=seconds))
    else:
        logger.debug("Sleeping " + str(seconds) + " seconds")
        time.sleep(seconds)


def wait_until(minutes, seconds, delta=None):
    """
    Waits until it is 00:minutes:seconds UTC time.
    :return timestamp until this function blocked
    """
    if mode_selector.simulation_mode:
        if not simulation.sim_time:  # first setup
            wait_until_timestamp = simulation.sim_day.replace(
                minute=minutes, second=seconds)
        else:
            wait_until_timestamp = simulation.sim_time.replace(
                minute=minutes, second=seconds)

        logger.info('Simulating wait until ' + str(wait_until_timestamp) +
                    ' (' + str(wait_until_timestamp.tzinfo) + ') ...')

        return simulation.skip_sim_time(wait_until_timestamp)
    elif mode_selector.test_mode:  # test mode: just wait one second here
        wait_timestamp = (datetime.now() + timedelta(seconds=1)
                          ).replace(microsecond=0)

        logger.info('Waiting until ' + str(wait_timestamp) +
                    ' (' + str(wait_timestamp.tzinfo) + ') ...')

        pause.until(wait_timestamp)

        result = wait_timestamp.replace(tzinfo=pytz.UTC).replace(microsecond=0)
    else:  # wait until 0:minutes:seconds UTC
        wait_timestamp = datetime.utcnow()

        # make datetime object aware of the UTC timezone
        wait_timestamp = wait_timestamp.replace(tzinfo=pytz.UTC)

        if delta:
            wait_timestamp += delta

        wait_timestamp = wait_timestamp.replace(hour=0, minute=minutes, second=seconds,
                                                microsecond=0)

        logger.info('Waiting until ' + str(wait_timestamp) +
                    ' (' + str(wait_timestamp.tzinfo) + ') ...')

        pause.until(wait_timestamp)

        result = datetime.now().replace(tzinfo=pytz.UTC).replace(microsecond=0)

    logger.info('Waiting finished - rebalancing happening now!')

    # returns until when this function blocked
    return result


def find_symbols_with_etf_tokens(tickers, token_type):
    """
    Based on given tickers, this function returns all PERP tokens that have a related BULL/BEAR/HEDGE token.
    :param tickers Tickers to evaluate
    :return List of PERP tokens that have a related BULL/BEAR/HEDGE token
    """

    if token_type == "BULL":
        # BTC-PERP has the BULL token "BULL/USD"
        symbols_with_etf_tokens = [("BTC-PERP", "BULL/USD")]
    elif token_type == "BEAR":
        # BTC-PERP has the BEAR token "BEAR/USD"
        symbols_with_etf_tokens = [("BTC-PERP", "BEAR/USD")]
    elif token_type == "HEDGE":
        # BTC-PERP has the HEDGE token "HEDGE/USD"
        symbols_with_etf_tokens = [("BTC-PERP", "HEDGE/USD")]
    else:
        raise Exception("Invalid token type: " + token_type)

    for symbol in tickers:
        if symbol.endswith(token_type + "/USD"):
            if symbol[:-(len(token_type)+len("/USD"))] + "-PERP" in tickers:
                symbols_with_etf_tokens.append(
                    (symbol[:-(len(token_type)+len("/USD"))] + "-PERP", symbol))
        elif symbol.endswith(token_type + "/USDT"):
            if symbol[:-(len(token_type)+len("/USDT"))] + "-PERP" in tickers:
                symbols_with_etf_tokens.append(
                    (symbol[:-(len(token_type)+len("/USDT"))] + "-PERP", symbol))

    # logger.debug("Symbols with " + token_type + " tokens: " +
    #             str(symbols_with_etf_tokens))

    return symbols_with_etf_tokens
