class TooLateException(BaseException):
    """
    Raised if the verify_time_is_before method detects that it's too late.
    """

    def __init__(self, now, verify_timestamp):
        self.now = now
        self.verify_timestamp = verify_timestamp


class SimulationFinished(BaseException):
    """
    Raised when simulation is finished.
    """
    pass


class EndOfSimulationDataException(BaseException):
    """
    Raised when simulation reached end of sim_data (sim_data_counter == len(sim_data)).
    """
    pass
