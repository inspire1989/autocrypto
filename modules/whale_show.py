from modules.misc.logger import logger
import json
from datetime import datetime

import plotly.graph_objects as go
from plotly.subplots import make_subplots


def plot_data(config, transactions, currency, filter_by_addresses=False, filter_by_single_address=None):
    logger.info("Plotting transactions for currency " + currency +
                (' with address filter' if filter_by_addresses else '') +
                (' with single address filter ' + filter_by_single_address if filter_by_single_address else ''))

    fig = make_subplots(
        rows=3, cols=1,
        # shared_xaxes=True,
        specs=[[{"type": "scatter"}],
               [{"type": "scatter"}],
               [{"type": "scatter"}]]
    )

    if currency == 'btc':
        color = 'rgb(255, 255, 0)'
    elif currency == 'usdt':
        color = 'rgb(0, 0, 255)'
    else:
        color = 'rgb(0, 0, 0)'

    logger.info('- Filtering')

    # filter by currency
    if currency != 'all_except_usdt':
        transactions = [
            transaction for transaction in transactions if transaction['symbol'] == currency]
    else:
        # if all_except_usdt currencies shall be listed, remove usdt
        transactions = [
            transaction for transaction in transactions if transaction['symbol'] != 'usdt']

    # filter by addresses
    if filter_by_addresses:
        transactions = [transaction for transaction in transactions
                        if (transaction['from']['address'] in config.address_whitelist[currency])
                        or (transaction['to']['address'] in config.address_whitelist[currency])]

    # filter by single address
    if filter_by_single_address:
        transactions = [transaction for transaction in transactions
                        if (transaction['from']['address'] in filter_by_single_address)
                        or (transaction['to']['address'] in filter_by_single_address)]

    # filter only transactions to and from exchanges
    transactions = [transaction for transaction in transactions if
                    (transaction['to']['owner_type'] ==
                     'exchange' and transaction['from']['owner_type'] != 'exchange')
                    or
                    (transaction['from']['owner_type'] == 'exchange' and transaction['to']['owner_type'] != 'exchange')]

    logger.info('- Preparing data')

    ### Histogram (hours)
    # dict: {"day.month.year hour": "total usdt equivalent transfered to and from exchanges"}
    histogram_hours = {datetime.fromtimestamp(t['timestamp']).strftime(
        '%d.%m.%Y %H:xx'): 0 for t in transactions}

    for t in transactions:
        if t['to']['owner_type'] == 'exchange' and t['from']['owner_type'] != 'exchange':
            histogram_hours[datetime.fromtimestamp(t['timestamp']).strftime(
                '%d.%m.%Y %H:xx')] += t['amount_usd'] * t['transaction_count']
        elif t['from']['owner_type'] == 'exchange' and t['to']['owner_type'] != 'exchange':
            histogram_hours[datetime.fromtimestamp(t['timestamp']).strftime(
                '%d.%m.%Y %H:xx')] -= t['amount_usd'] * t['transaction_count']

    fig.add_trace(go.Scatter(x=[k for k in histogram_hours.keys()],
                             y=[v for v in histogram_hours.values()],
                             name='Histogram (days) of transactions to and from exchanges for currency: ' + currency),
                  row=1, col=1)

    ### Histogram (days)
    # dict: {"day.month.year": "total usdt equivalent transfered to and from exchanges"}
    histogram_days = {datetime.fromtimestamp(
        t['timestamp']).strftime('%d.%m.%Y'): 0 for t in transactions}

    for t in transactions:
        if t['to']['owner_type'] == 'exchange' and t['from']['owner_type'] != 'exchange':
            histogram_days[datetime.fromtimestamp(t['timestamp']).strftime(
                '%d.%m.%Y')] += t['amount_usd'] * t['transaction_count']
        elif t['from']['owner_type'] == 'exchange' and t['to']['owner_type'] != 'exchange':
            histogram_days[datetime.fromtimestamp(t['timestamp']).strftime(
                '%d.%m.%Y')] -= t['amount_usd'] * t['transaction_count']

    fig.add_trace(go.Scatter(x=[k for k in histogram_days.keys()],
                             y=[v for v in histogram_days.values()],
                             name='Histogram (hours) of transactions to and from exchanges for currency: ' + currency),
                  row=2, col=1)

    # Aggregated movements (hours)
    # dict: {"day.month.year hour": "aggregated usdt equivalent transfered to and from exchanges"}
    aggregated_hours = dict()

    for t in transactions:
        if t['to']['owner_type'] == 'exchange' and t['from']['owner_type'] != 'exchange':
            aggregated_hours[datetime.fromtimestamp(t['timestamp']).strftime('%d.%m.%Y %H:xx')] = (list(
                aggregated_hours.values())[-1] if len(aggregated_hours) > 0 else 0) + t['amount_usd'] * t['transaction_count']
        elif t['from']['owner_type'] == 'exchange' and t['to']['owner_type'] != 'exchange':
            aggregated_hours[datetime.fromtimestamp(t['timestamp']).strftime('%d.%m.%Y %H:xx')] = (list(
                aggregated_hours.values())[-1] if len(aggregated_hours) > 0 else 0) - t['amount_usd'] * t['transaction_count']

    fig.add_trace(go.Scatter(x=[k for k in aggregated_hours.keys()],
                             y=[v for v in aggregated_hours.values()],
                             name='Aggregated movements (hours) of transactions to and from exchanges for currency: ' + currency),
                  row=3, col=1)

    # Single transactions

    # # x: date
    # x_values = [datetime.fromtimestamp(transaction['timestamp']).strftime('%d.%m.%Y %H:%M:%S')
    #             for transaction in transactions]
    #
    # # y: usdt equivalent.
    # # Positive values: transfer was made to exchange. Negative value: transfer was made from exchange away.
    # y_values = []
    # for t in transactions:
    #     if t['to']['owner_type'] == 'exchange' and t['from']['owner_type'] != 'exchange':
    #         y_values.append(t['amount_usd'] * t['transaction_count'])
    #     elif t['from']['owner_type'] == 'exchange' and t['to']['owner_type'] != 'exchange':
    #         y_values.append(-1.0 * t['amount_usd'] * t['transaction_count'])
    #
    # fig.add_trace(go.Scatter(x=x_values,
    #                          y=y_values,
    #                          name='All transactions to and from exchanges for currency: ' + currency,
    #                          mode='markers',
    #                          marker_color=color),
    #               row=4, col=1)

    # Show figure
    fig.update_layout(title_text="Whale statistics for currency: " + currency +
                                 (' with address filter' if filter_by_addresses else '') +
                                 (' with single address filter ' +
                                  filter_by_single_address if filter_by_single_address else ''),
                      height=2400,
                      width=1400)
    fig.show()


def whale_show(config, parser, args):
    if args.file is None:
        parser.error("File must be provided with --file/-f.")

    logger.info("Loading file ...")
    data_string = '{"transactions": ['

    line_number = 0
    with open(args.file, 'r', encoding='utf-8') as f:
        data_string += f.read().replace("\n", ",\n")

    data_string = data_string[:-2] + ']}'

    logger.info("Parsing json ...")
    result = json.loads(data_string)

    # remove duplicates
    result['transactions'] = {
        t['id']: t for t in result['transactions']}.values()

    # sort by date
    result['transactions'] = sorted(
        result['transactions'], key=lambda x: x['timestamp'])

    # show known types of owner_types
    owner_types = set()
    for t in result['transactions']:
        owner_types.add(t['to']['owner_type'])
    logger.info('Known owner_types: ' + str(owner_types))

    plot_data(config=config,
              transactions=result['transactions'], currency='all_except_usdt')
    plot_data(config=config,
              transactions=result['transactions'], currency='btc')
    plot_data(config=config,
              transactions=result['transactions'], currency='xrp')
    plot_data(config=config,
              transactions=result['transactions'], currency='usdt')

    plot_data(config=config,
              transactions=result['transactions'], currency='btc', filter_by_addresses=True)

    # Interesting addresses in top 50
    plot_data(config=config, transactions=result['transactions'], currency='btc',
              filter_by_single_address='1P5ZEDWTKTFGxQjZphgWPQUpe554WKDfHQ')
    plot_data(config=config, transactions=result['transactions'], currency='btc',
              filter_by_single_address='3LCGsSmfr24demGvriN4e3ft8wEcDuHFqh')
    #plot_data(config=config, transactions=result['transactions'], currency='btc', filter_by_single_address='1aXzEKiDJKzkPxTZy9zGc3y1nCDwDPub2')
    #plot_data(config=config, transactions=result['transactions'], currency='btc', filter_by_single_address='3EiEN1JJCudBgAwL7c6ajqnPzx9LrK1VT6')
