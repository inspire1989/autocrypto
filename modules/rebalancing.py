from concurrent.futures.thread import ThreadPoolExecutor
from hmac import new
from modules.misc.Order import Order, OrderType
from strategies.stop.StopStrategyInterface import StopStrategyParams
from strategies.start.StartStrategyInterface import StartStrategyParams
from strategies.stop.TargetLeverageReachedStopStrategy import TargetLeverageReachedStopStrategy
from strategies.stop.ProtectionStopStrategy import ProtectionStopStrategy
from modules.misc.Exceptions import EndOfSimulationDataException

from modules.simulation.simulation import SimulationFinished, get_data_for_day_from_db
from modules.misc.misc import TooLateException, find_symbols_with_etf_tokens, sleep_for, verify_time_is_before, wait_until
from modules.misc.exchange_functions import get_order_books, open_position, close_position, get_balance, get_etf_token_informations, get_ohlcv, get_order_book, get_tickers, init_exchange, market_close_all_positions, open_position
from modules.misc.logger import logger
import config.rebalancing_symbols as rebalancing_symbols
import modules.misc.thread_exception_handler as thread_exception_handler
import modules.misc.mariadb_interface as mariadb_interface
import modules.misc.mode_selector as mode_selector
import modules.simulation.simulation as simulation

from strategies.start.BullLevDiffStartStrategy import BullLevDiffStartStrategy

from datetime import datetime, timedelta
import time
import pytz
import os
import traceback


def self_test():
    pass


def capture_rebalancing(symbols, rebalancing_start_time):
    logger.info("Capturing rebalance")

    i = 0
    seconds_increment = 3  # 2 seconds: "too many requests"
    max_minutes = 4
    max_iterations = int(max_minutes*60/seconds_increment)

    while True:
        logger.info("Step " + str(i+1) + "/" + str(max_iterations))

        tickers = get_tickers(update_db=True)

        get_etf_token_informations(
            rebalancing_symbols.get_reference_symbols(with_suffix=False), tickers, update_db=True)

        # Fetch initial order books for all "X-PERP" tokens
        get_order_books(depth=100, grouping=100, symbols=rebalancing_symbols.get_reference_symbols(
                        with_suffix=False), update_db=True)

        # multi-threaded: fetch new data for each symbol
        with ThreadPoolExecutor(len(symbols)) as e:
            for symbol in symbols:
                # once for every minute
                if i % (60/seconds_increment) == 0 or i == max_iterations-1:
                    # gets the ohlcv of the last completed minute
                    e.submit(get_ohlcv, symbol, "1m", True).add_done_callback(
                        thread_exception_handler.thread_exception_check_callback)

        # capture data until 0:06:00:000000 (or until max_iterations reached)
        if i == max_iterations-1:
            logger.info("max_iterations for capturing reached")
            break
        else:
            if mode_selector.test_mode:
                # after 2 minutes abort
                if datetime.now().replace(tzinfo=pytz.UTC) > rebalancing_start_time + timedelta(minutes=2):
                    logger.info("Time limit for capturing reached")
                    break
            else:
                # abort at 0:06:00:000000
                if datetime.utcnow() > datetime.utcnow().replace(
                        hour=0, minute=6, second=0, microsecond=0):
                    logger.info("Time limit for capturing reached")
                    break

            wait_until = rebalancing_start_time.replace(
                microsecond=0) + timedelta(seconds=seconds_increment)*(i+1)
            now = datetime.now().replace(tzinfo=pytz.UTC)

            if now < wait_until:
                logger.info(
                    "Wait until " + str(wait_until))
                time.sleep((wait_until - now).seconds +
                           (wait_until - now).microseconds / 1000000)
            else:
                logger.info("Need to catch up ... (" + str(wait_until) + ")")

        i = i+1


def sniper_rebalancing(tickers, token_informations, initial_order_books):

    start_strategy = BullLevDiffStartStrategy()
    protection_stop_strategy = ProtectionStopStrategy()
    stop_strategy1 = TargetLeverageReachedStopStrategy()

    start_strategy_params = StartStrategyParams(
        tickers=tickers, token_informations=token_informations, order_books=initial_order_books)
    start_strategy_result = start_strategy.get_start(start_strategy_params)

    if not start_strategy_result.chosen_token:
        logger.warning("No token chosen, skipping rebalancing")
        return

    if start_strategy_result.order.symbol in tickers:
        logger.info("Start price of the underlying token " + start_strategy_result.order.symbol +
                    ": $" + str(tickers[start_strategy_result.order.symbol]["last"]))
    else:
        raise Exception("Oops... chosen token not found in tickers?!")

    # rebalancing starts at 01:55, give it some seconds beforehand to be sure
    wait_until_time = wait_until(minutes=1, seconds=55)

    verify_time_is_before(wait_until_time.replace(second=57, microsecond=0))

    open_position(start_strategy_result.order)

    # capture rebalancing steps
    while True:
        tickers = get_tickers(max_sim_time_increase=timedelta(seconds=5))
        if not tickers:
            logger.warning("No tickers found")
        else:
            if start_strategy_result.order.symbol in tickers:
                logger.debug("Current price of the underlying token " + start_strategy_result.order.symbol +
                             ": $" + str(tickers[start_strategy_result.order.symbol]["last"]))
            else:
                close_position(start_strategy_result.order)
                raise Exception("Oops... chosen token not found in tickers?!")

        token_informations = get_etf_token_informations(
            rebalancing_symbols.get_reference_symbols(with_suffix=False), tickers)

        if token_informations:
            for token_information in token_informations:
                if token_information["name"] == start_strategy_result.chosen_token:
                    logger.debug(token_information["name"] + ": current leverage " + str(
                        token_information["currentLeverage"]) + ", target leverage " + str(
                        token_information["leverage"]))
        else:
            logger.warning("No ETF token informations found")

        # get order books for all "X-PERP" tokens
        order_books = get_order_books(
            max_sim_time_increase=timedelta(seconds=5))

        stop_strategy_params = StopStrategyParams(rebalancing_sniper_start_time=wait_until_time,
                                                  chosen_token=start_strategy_result.chosen_token,
                                                  order=start_strategy_result.order,
                                                  tickers=tickers,
                                                  token_informations=token_informations,
                                                  order_books=order_books
                                                  )
        # if stop_strategy1.check(stop_strategy_params):
        #    close_position(start_strategy_result.order)
        #    break

        if protection_stop_strategy.check(stop_strategy_params):
            close_position(start_strategy_result.order)
            break

        sleep_for(1)

    tickers = get_tickers(max_sim_time_increase=timedelta(seconds=5))
    if tickers:
        if start_strategy_result.order.symbol in tickers:
            logger.info("End price of the underlying token " + start_strategy_result.order.symbol +
                        ": $" + str(tickers[start_strategy_result.order.symbol]["last"]))
        else:
            raise Exception("Oops... chosen token not found in tickers?!")


def init_symbols(tickers):
    rebalancing_symbols.clear_symbols()

    # find all symbols that have corresponding BULL/BEAR/HEDGE tokens
    symbols_with_bull_tokens = find_symbols_with_etf_tokens(tickers, "BULL")
    rebalancing_symbols.set_symbols(symbols_with_bull_tokens)

    symbols_with_bear_tokens = find_symbols_with_etf_tokens(tickers, "BEAR")
    rebalancing_symbols.set_symbols(symbols_with_bear_tokens)

    symbols_with_hedge_tokens = find_symbols_with_etf_tokens(tickers, "HEDGE")
    rebalancing_symbols.set_symbols(symbols_with_hedge_tokens)


def emergency_abort():
    mariadb_interface.lock.acquire()  # ensure db is not used currently
    market_close_all_positions()  # TODO: implement
    os._exit(1)


def init(config):
    init_exchange(config)

    # install callback for exception handling (within threads only!)
    thread_exception_handler.exception_callback = emergency_abort


def rebalancing(config, parser, args):

    self_test()

    init(config)

    if args.sniper:
        logger.info("Sniping FTX rebalancing")

        last_balance = get_balance(update_db=False)
        logger.info('Total account balance: $%.2f' % last_balance)
        initial_balance = last_balance

        number_of_losing_days = 0
        number_of_winning_days = 0
        number_of_constant_days = 0

        if args.simulate:
            mariadb_interface.connect_db()

            start_of_simulation_time = time.time()
    elif args.capture:
        logger.info("Capturing FTX rebalancing")
    else:
        raise("Unsupported")

    try:
        while True:
            print()

            try:
                if args.simulate:
                    # get new simulation data for next day (if no data was found for this day, go to the next day ...)
                    while not get_data_for_day_from_db(timedelta(days=1)):
                        print()

                start_time = wait_until(
                    minutes=1, seconds=50, delta=timedelta(days=1))

                logger.info("Rebalancing starts")

                if args.capture:
                    mariadb_interface.connect_db()

                # Fetch initial tickers (incl retry)
                tickers = get_tickers(
                    max_sim_time_increase=timedelta(seconds=5), update_db=True)

                while not tickers:
                    logger.warning(
                        "Could not retrieve initial tickers, retrying...")
                    verify_time_is_before(
                        start_time.replace(second=57, microsecond=0))
                    sleep_for(1)
                    tickers = get_tickers(
                        max_sim_time_increase=timedelta(seconds=5), update_db=True)

                # Initialize exchange symbols
                init_symbols(tickers)

                # Fetch initial ETF token informations (incl retry)
                token_informations = get_etf_token_informations(symbols=rebalancing_symbols.get_reference_symbols(
                    with_suffix=False), tickers=tickers, update_db=True, max_sim_time_increase=timedelta(seconds=5))

                while not token_informations:
                    logger.warning(
                        "Could not retrieve initial token informations, retrying...")
                    verify_time_is_before(
                        start_time.replace(second=57, microsecond=0))
                    sleep_for(1)
                    token_informations = get_etf_token_informations(symbols=rebalancing_symbols.get_reference_symbols(
                        with_suffix=False), tickers=tickers, update_db=True, max_sim_time_increase=timedelta(seconds=5))

                # Fetch initial order books for all "X-PERP" tokens (incl retry)
                all_order_books = get_order_books(
                    max_sim_time_increase=timedelta(seconds=5), depth=100, grouping=100, symbols=rebalancing_symbols.all_symbols, update_db=True)

                while not all_order_books:
                    logger.warning(
                        "Could not retrieve initial order books, retrying...")
                    verify_time_is_before(
                        start_time.replace(second=57, microsecond=0))
                    sleep_for(1)
                    all_order_books = get_order_books(
                        max_sim_time_increase=timedelta(seconds=5), update_db=True)

                # ensure it's not too late now...
                verify_time_is_before(
                    start_time.replace(second=59, microsecond=0))

                if args.sniper:
                    sniper_rebalancing(
                        tickers=tickers,
                        token_informations=token_informations,
                        initial_order_books=all_order_books)

                    # get balance change
                    new_balance = get_balance()

                    logger.info('Total account balance: $%.2f' % new_balance)
                    logger.info('--> Change of %.2f%% during this rebalancing' %
                                ((new_balance-last_balance)/last_balance*100))

                    if last_balance > new_balance:
                        number_of_losing_days += 1
                    elif last_balance < new_balance:
                        number_of_winning_days += 1
                    else:
                        number_of_constant_days += 1

                    last_balance = new_balance
                elif args.capture:
                    # capture all tradesymbols from the symbol_map
                    capture_rebalancing(
                        rebalancing_symbols.all_symbols, start_time)
            except KeyboardInterrupt:  # completely abort
                traceback.print_exc()
                raise KeyboardInterrupt()
            except TooLateException as e:
                logger.error("Caught TooLateException with now=" +
                             str(e.now) + ", verify_timestamp=" + str(e.verify_timestamp))
                traceback.print_exc()
                logger.error("Skipping current rebalancing")
            except EndOfSimulationDataException as e:
                logger.error(
                    "End of simulation data reached! This shouldn't happen: Either the rebalancing sniper didn't come to an end during the rebanancing or the data set doesn't have enough rows!")
                traceback.print_exc()
                exit(1)
            except SimulationFinished:
                logger.info("### Simulation finished! ###")

                # get balance change
                new_balance = get_balance()

                logger.info("Initial account balance: $%.2f" % initial_balance)
                logger.info("End account balance: $%.2f" % new_balance)
                logger.info("--> Change of %.2f%% during simulation" %
                            ((new_balance-initial_balance)/initial_balance*100))
                logger.info("Number of simulated days: " +
                            str(simulation.sim_day_counter))
                if simulation.sim_day_counter > 0:
                    logger.info("Average change per day: %.2f%%" %
                                float(((new_balance - initial_balance)/initial_balance*100) / simulation.sim_day_counter))

                    print()
                    logger.info("Number of days without trades: " +
                                str(number_of_constant_days))
                    logger.info("Number of winning days: " +
                                str(number_of_winning_days))
                    logger.info("Number of losing days: " +
                                str(number_of_losing_days))

                    if number_of_winning_days > 0:
                        logger.info("--> Ratio win/loose: 1/" +
                                    str(round(number_of_losing_days/number_of_winning_days, 2)))
                    else:
                        logger.info("--> Ratio win/loose: 1/INF")

                print()
                logger.info("Simulation took %.2f seconds" %
                            float(time.time() - start_of_simulation_time))

                exit(0)
            except Exception as e:
                logger.error("Caught exception: " + str(e) +
                             ". Aborting current day's rebalancing.")
                traceback.print_exc()
                emergency_abort()
            finally:
                if args.capture:
                    mariadb_interface.disconnect_db()
    except KeyboardInterrupt:
        logger.warning("KeyboardInterrupt")
        if args.sniper:
            logger.critical("Open positions on exchange haven't been closed!")
    except Exception:
        logger.critical(traceback.format_exc())
        emergency_abort()
