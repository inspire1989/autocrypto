from pause import seconds
from modules.misc.Exceptions import EndOfSimulationDataException, SimulationFinished

from modules.misc.mariadb_interface import db_get_data_from_table
from modules.misc.logger import logger

from dateutil import parser
from datetime import datetime, timedelta
import cysimdjson

# first data in db: 16.06.2021 (5 seconds between rows)
# 08.07.2021: changed to min time between rows (2-3 seconds)
# 09.07.2021: changed to 3 seconds between rows
start_day = parser.parse("2021-06-16")
#start_day = parser.parse("2021-07-01")

# Balance (initial, will be updated during simulation by using the open_position() or close_position() function) [USD]
balance = 1000

# current day of simulation (incremented automatically)
sim_day = start_day

# current simulation time (incremented automatically)
sim_time = None

# data for the current day (n elements).
sim_data = None

# sim_data holds n elements. The data_counter here points to the next data among the n elements that shall be processed.
sim_data_counter = 0

# counts how many days have been simulated
sim_day_counter = 0

# definitions for raw data table
ID = 0
EXCHANGE = 1
TIMESTAMP = 2
METADATA = 3
INFORMATIONTYPE = 4
VALUE = 5


def get_data_for_day_from_db(sim_day_increment=None):
    """
    Get data for simulation from db starting at start_day.
    :param sim_day_increment: increment sim_day by this timedelta before retrieving data from db.
    :return True if data for the day was found, False if not.
    """
    global sim_day
    global sim_time
    global sim_data
    global sim_data_counter
    global sim_day_counter

    if sim_day_increment and sim_day_counter > 0:
        sim_day += sim_day_increment

    if sim_day >= datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1):
        raise SimulationFinished()

    sim_time = sim_day
    logger.info("Fetching simulation data from db for " + str(sim_day))

    sim_data = db_get_data_from_table(
        table="rebalancing_raw", exchange="ftx", day=sim_day)

    if not sim_data or len(sim_data) == 0:
        logger.warning("No data found for " + str(sim_day))
        return False
    else:  # data found for sim_day in db
        sim_data_counter = 0
        sim_day_counter += 1

        # logger.debug("New sim time: " + str(sim_time))

        return True


def get_next(informationType, max_sim_time_increase=timedelta(seconds=5), meta_data_filter=None):
    """
    Get next data of any type from db.
    This function does not advance the sim_data_counter or the sim_time!

    :param informationType Get data with this information type only.
    :param max_sim_time_increase: Maximum timedelta (from sim_time) in the future of the data to retrieve (5 seconds default).
                                  If None, the this function will continue to look into the future until it finds the informationType.
                                  Only data that is at maximum max_sim_time_increase into the future will be retrieved.
                                  None will be returned if no data within this timespan is found.
    :param meta_data_filter: Filter function for the meta data column. If specified, this filter must return True in order to be a match.
    :return Next data of informationType starting at sim_time. If max_sim_time_increase specified upper time limit is sim_time+max_sim_time_increase. If nothing was found or end of day is reached, returns None.
    """

    global sim_day
    global sim_data
    global sim_data_counter
    global sim_time

    # end of the day already reached, no more data available to look for
    if sim_data_counter == len(sim_data):
        raise EndOfSimulationDataException()

    local_data_counter = sim_data_counter
    while True:
        # end of the day reached, nothing found
        if local_data_counter == len(sim_data):
            return None

        current_data = sim_data[local_data_counter]
        timestamp = current_data[TIMESTAMP]

        # if max_sim_time_increase defined: search for data until sim_time + max_sim_time_increase is reached. If no data found until there, return None
        if max_sim_time_increase:
            if timestamp > sim_time + max_sim_time_increase:
                return None

        if current_data[INFORMATIONTYPE] != informationType:
            local_data_counter += 1
        else:
            if not meta_data_filter:
                return current_data
            else:
                if meta_data_filter(current_data[METADATA]):
                    return current_data
                else:
                    local_data_counter += 1


def skip_sim_time(target_time):
    """
    Skip simulation data until target_time is reached. It jumps to a target_time in the future and adjusts sim_time and sim_data_counter.
    If the target_time is < sim_time, nothing is done here.
    :param target_time target timestamp until where the simulation shall jump
    :return new sim_time
    """
    global sim_day
    global sim_data
    global sim_data_counter
    global sim_time

    # end of the day already reached, nothing found
    if sim_data_counter != len(sim_data):
        while True:
            current_data = sim_data[sim_data_counter]
            sim_time = current_data[TIMESTAMP]

            if sim_time < target_time:
                sim_data_counter += 1

                # no more data available for the current day, abort
                if sim_data_counter == len(sim_data):
                    break
                    # get_data_for_day_from_db(timedelta(days=1))
            else:
                break

    sim_time = target_time

    # logger.debug("New sim time: " + str(sim_time))

    return sim_time


def get_next_tickers(max_sim_time_increase=timedelta(seconds=5)):
    """
    Get next ticker data.
    :param max_sim_time_increase: Maximum timedelta (from sim_time) in the future of the data to retrieve (5 seconds default).
                                  If None, the this function will continue to look into the future until it finds the informationType.
                                  Only data that is at maximum max_sim_time_increase into the future will be retrieved.
                                  None will be returned if no data within this timespan is found.
    """

    current_data = get_next(
        informationType="tickers", max_sim_time_increase=max_sim_time_increase)

    if current_data:
        parser = cysimdjson.JSONParser()
        try:
            return parser.parse(str.encode(current_data[VALUE]))
        except ValueError as e:
            json_file_path = "/tmp/json.txt"
            with open(json_file_path, "w") as file:
                file.write(current_data[VALUE])

            logger.error("Caught ValueError exception: " + str(e) +
                         " when parsing JSON. String given to JSON parser was saved at " + str(json_file_path))
            raise e
    else:
        return None


def get_next_etf_token_informations(max_sim_time_increase=timedelta(seconds=5)):
    """
    Get ETF token data.
    :param max_sim_time_increase: Maximum timedelta (from sim_time) in the future of the data to retrieve (5 seconds default).
                                  If None, the this function will continue to look into the future until it finds the informationType.
                                  Only data that is at maximum max_sim_time_increase into the future will be retrieved.
                                  None will be returned if no data within this timespan is found.
    """

    current_data = get_next(informationType="get_etf_token_informations",
                            max_sim_time_increase=max_sim_time_increase)

    if current_data:
        parser = cysimdjson.JSONParser()
        try:
            return parser.parse(str.encode(current_data[VALUE]))
        except ValueError as e:
            json_file_path = "/tmp/json.txt"
            with open(json_file_path, "w") as file:
                file.write(current_data[VALUE])

            logger.error("Caught ValueError exception: " + str(e) +
                         " when parsing JSON. String given to JSON parser was saved at " + str(json_file_path))
            raise e
    else:
        return None


def get_next_order_book(symbol, max_sim_time_increase=timedelta(seconds=5)):
    """
    Get next order book data.
    :param symbol: Order book for this symbol shall be retrieved.
    :param max_sim_time_increase: Maximum timedelta (from sim_time) in the future of the data to retrieve (5 seconds default).
                                  If None, the this function will continue to look into the future until it finds the informationType.
                                  Only data that is at maximum max_sim_time_increase into the future will be retrieved.
                                  None will be returned if no data within this timespan is found.
    """
    parser = cysimdjson.JSONParser()

    def meta_data_filter(meta_data: str):
        meta_data_json = parser.parse(str.encode(meta_data))
        return meta_data_json["symbol"] == symbol

    current_data = get_next(
        informationType="order_book", max_sim_time_increase=max_sim_time_increase, meta_data_filter=meta_data_filter)

    if current_data:
        try:
            return parser.parse(str.encode(current_data[VALUE]))
        except ValueError as e:
            json_file_path = "/tmp/json.txt"
            with open(json_file_path, "w") as file:
                file.write(current_data[VALUE])

            logger.error("Caught ValueError exception: " + str(e) +
                         " when parsing JSON. String given to JSON parser was saved at " + str(json_file_path))
            raise e
    else:
        return None


def get_next_order_books(max_sim_time_increase=timedelta(seconds=5)):
    """
    Get next order books data (all order books in one row).
    :param max_sim_time_increase: Maximum timedelta (from sim_time) in the future of the data to retrieve (5 seconds default).
                                  If None, the this function will continue to look into the future until it finds the informationType.
                                  Only data that is at maximum max_sim_time_increase into the future will be retrieved.
                                  None will be returned if no data within this timespan is found.
    """

    current_data = get_next(
        informationType="order_books", max_sim_time_increase=max_sim_time_increase)

    if current_data:
        try:
            parser = cysimdjson.JSONParser()
            return parser.parse(str.encode(current_data[VALUE]))
        except ValueError as e:
            json_file_path = "/tmp/json.txt"
            with open(json_file_path, "w") as file:
                file.write(current_data[VALUE])

            logger.error("Caught ValueError exception: " + str(e) +
                         " when parsing JSON. String given to JSON parser was saved at " + str(json_file_path))
            raise e
    else:
        return None
