import pandas
import plotly.express as px
from sqlalchemy import create_engine
import plotly.graph_objects as go
import plotly.graph_objects as go
import cysimdjson as cysimdjson
from modules.misc.logger import logger
from modules.misc.mariadb_interface import *

timeframe = '5m'
engine = create_engine("mariadb://root:root@localhost/autocrypto")

json_parser = cysimdjson.JSONParser()

# symbols = ["BTC-PERP", "ETH-PERP", "LINK-PERP", "XRP-PERP",
#            "ADA-PERP", "DOT-PERP", "FTT-PERP", "BNB-PERP", "SXP-PERP"]
symbols = ["BTC-PERP"]


def extend_single(json, symbol, param):
    if symbol in json:
        if param in json[symbol]:
            return json[symbol][param]

    return None


def analyze_order_book(symbol, fig):
    logger.debug("Preparing order book for " + symbol)
    df_order_books_all = pandas.read_sql(
        "SELECT * FROM recording WHERE informationType = 'order_books' AND JSON_CONTAINS(metadata, 100, '$.depth') AND JSON_CONTAINS(metadata, 50, '$.grouping');", engine)

    df = df_order_books_all[[
        'id', 'exchange', 'timestamp', 'metadata']].copy()

    df['asks'] = [extend_single(json_parser.parse(str.encode(value)), symbol, 'asks')
                  for key, value in df_order_books_all['value'].items()]
    df['asks'] = [list([list(ask) if ask else None for ask in asks])
                  if asks else None for asks in df["asks"]]

    df['bids'] = [extend_single(json_parser.parse(str.encode(value)), symbol, 'bids')
                  for value in df_order_books_all['value']]
    df['bids'] = [list([list(bid) if bid else None for bid in bids])
                  if bids else None for bids in df["bids"]]

    # print(df)

    # df = pandas.DataFrame()
    # for i in range(10):
    #     df = pandas.concat([df, pandas.json_normalize(requests.get(
    #         "https://api.cryptowat.ch/markets/kraken/btcusd/orderbook").json()["result"]).assign(timestamp=pandas.to_datetime("now"))])
    #     time.sleep(1)

    d_asks = df.loc[:, ["timestamp", "asks"]].explode("asks").assign(
        price=lambda d: d["asks"].apply(lambda a: a[0] if a else None),
        size=lambda d: d["asks"].apply(lambda a: a[1] if a else None),
    )

    d_bids = df.loc[:, ["timestamp", "bids"]].explode("bids").assign(
        price=lambda d: d["bids"].apply(lambda a: a[0] if a else None),
        size=lambda d: d["bids"].apply(lambda a: a[1] if a else None),
    )

    d = d_asks.merge(d_bids, how='outer')

    fig.add_trace(go.Histogram2d(
        x=d["timestamp"],
        z=d["size"],
        y=d["price"],
        ybins=dict(
            start=30000,
            end=35000,
            size=10
        ),
        # nbinsy=1000,
        nbinsx=1000,
        colorscale='YlGnBu'))

    fig.add_trace(go.Scatter(
        x=d["timestamp"], y=d["price"], mode='markers', marker=dict(color=d["size"]), marker_symbol="square"))


def analyze(parser, args):
    logger.info("Analyzing data")

    for index, symbol in enumerate(symbols):
        logger.debug("Preparing " + timeframe + " ohlc data for " +
                     symbol + " [" + str(index+1) + "/" + str(len(symbols)) + "]")

        df_ohlcvs_all = pandas.read_sql(
            "SELECT * FROM recording WHERE informationType = 'ohlcvs' AND JSON_CONTAINS(metadata, '\"" + timeframe + "\"', '$.timeframe');", engine)

        df_ohlcvs = df_ohlcvs_all[[
            'id', 'exchange', 'timestamp', 'metadata']].copy()
        df_ohlcvs['open'] = [extend_single(json_parser.parse(str.encode(value)), symbol, 'open')
                             for value in df_ohlcvs_all['value']]
        df_ohlcvs['high'] = [extend_single(json_parser.parse(str.encode(value)), symbol, 'high')
                             for value in df_ohlcvs_all['value']]
        df_ohlcvs['low'] = [extend_single(json_parser.parse(str.encode(value)), symbol, 'low')
                            for value in df_ohlcvs_all['value']]
        df_ohlcvs['close'] = [extend_single(json_parser.parse(str.encode(value)), symbol, 'close')
                              for value in df_ohlcvs_all['value']]
        df_ohlcvs['volume'] = [extend_single(json_parser.parse(str.encode(value)), symbol, 'volume')
                               for value in df_ohlcvs_all['value']]

        fig = go.Figure()

        analyze_order_book(symbol, fig)

        fig.add_trace(go.Candlestick(x=df_ohlcvs['timestamp'],
                                     open=df_ohlcvs['open'],
                                     high=df_ohlcvs['high'],
                                     low=df_ohlcvs['low'],
                                     close=df_ohlcvs['close']))

        fig.update_xaxes(rangeslider_visible=True)
        fig.update_layout(
            title="OHLC " + timeframe + " data for " + symbol,
            title_x=0.5,
            yaxis_title='Price in USD',
            yaxis=dict(
                fixedrange=False
            ),
            paper_bgcolor='rgba(0,0,0,0)',
            # plot_bgcolor='rgba(0,0,0,0)'
        )
        fig.show()
