from modules.misc.logger import logger
from enum import Enum
import pandas as pd
import collections


class Direction(Enum):
    LONG = 0,
    SHORT = 1


def test(parser, args):
    if args.file is None:
        parser.error(
            "Provide a csv file with --file.")

    df = pd.read_csv(args.file)

    funds = 1000

    direction = Direction.LONG
    last_direction = Direction.LONG
    fail_history = collections.deque(maxlen=3)

    win_percentage = 1.00

    logger.info(
        f'Start funds: ${funds}, win percentage: {round((win_percentage))}%')
    for index, row in df.iterrows():
        print()
        direction_for_this_day = direction

        print(fail_history)
        if sum(fail_history) > 1:
            logger.warn('Fail protection enabled!')

        if last_direction == Direction.LONG:
            if row['Open']*(1+win_percentage/100) <= row['High']:
                if sum(fail_history) <= 1:
                    funds *= (1+win_percentage/100)
            else:
                if sum(fail_history) <= 1:
                    funds *= 1-(abs(row['Close'] - row['Open']))/row['Open']
                direction = Direction.SHORT
                logger.warn(
                    f"{row['Open time'].replace(' 00:00', '')}: Long failed, going short now")

            if row['Open'] < row['Close']:
                fail_history.append(0)
            else:
                fail_history.append(1)

        elif last_direction == Direction.SHORT:
            if row['Open']*(1-win_percentage/100) >= row['Low']:
                if sum(fail_history) <= 1:
                    funds *= (1+win_percentage/100)
            else:
                if sum(fail_history) <= 1:
                    funds *= 1-(abs(row['Close'] - row['Open']))/row['Open']
                direction = Direction.LONG
                logger.warn(
                    f"{row['Open time'].replace(' 00:00', '')}: Short failed, going long now")

            if row['Open'] > row['Close']:
                fail_history.append(0)
            else:
                fail_history.append(1)
        else:
            logger.info("Unclear trend, skipping")

        if row['Open'] < row['Close']:
            last_direction = Direction.LONG
        else:
            last_direction = Direction.SHORT

        logger.info(
            f"{row['Open time'].replace(' 00:00', '')} EOD: ${round(funds)} ({'LONG' if direction_for_this_day == Direction.LONG else 'SHORT'})")
