from datetime import timedelta
from config.rebalancing_symbols import *
from modules.misc.logger import logger
from modules.misc.mariadb_interface import *

import plotly.express as px


def create_empty_data_structure():
    """
    Creates an empty data structure:
        dict{"1:55": [], "1:56": [], ..., "5:59": []}
    :return new data structure
    """
    data = dict()

    for minute in range(1, 6):
        for second in range(0, 60):
            if minute == 1 and second < 55:
                continue

            data[str(minute) + ":" + str(second)] = []

    return data


def analyze_prices(start_date, end_date, delta_dates):
    logger.info("Analyzing prices")

    exchange = "ftx"

    symbols = db_get_all_symbols_from_table(table="rebalancing_price",
                                            exchange=exchange)

    logger.info("Found these symbols in db: " + str(symbols))

    for symbol in symbols:  # iterate symbols

        # data: holds absolute change of price in between seconds
        data = create_empty_data_structure()

        number_of_rows_found = 0
        for i in range(delta_dates.days + 1):  # iterate all days
            day = start_date + timedelta(days=i)  # current day

            logger.debug("Retrieving prices for " + str(day.year) +
                         "-" + str(day.month) + "-" + str(day.day))

            # get prices for the current day for the current symbol
            prices = db_get_prices_for_day(table="rebalancing_price",
                                           exchange=exchange, symbol=symbol, timestamp=day)
            logger.debug(symbol + ": " + str(len(prices)) + " rows found")

            # skip if no data for the current day available
            if len(prices) == 0:
                continue

            # all entries for the current day
            last_time = 0
            last_value = None
            for i, row in enumerate(prices):
                # print(row)
                # id = row[0]
                # exchange = row[1]
                # symbol = row[2]

                timestamp = row[3]
                minute = timestamp.minute
                second = timestamp.second

                time = str(minute) + ":" + str(second)

                if i != 0:
                    if time in data.keys():
                        if last_time in data.keys():
                            data[time].append(abs(row[4] - last_value))

                last_value = row[4]
                last_time = time

            number_of_rows_found += len(prices)

        if number_of_rows_found > 0:
            # bring it in shape for scatter plot (collection of xn, yn values including duplicate xn values for the timestamps where length(xn) == length(yn))
            x_values = []
            y_values = []
            for timestamp in data:
                for value in data[timestamp]:
                    x_values.append(timestamp)
                    y_values.append(value)

            fig = px.scatter(x=x_values, y=y_values)

            fig.update_layout(
                title="[" + exchange + "] " +
                "Absolute change in price for " + symbol +
                " based on " + str(number_of_rows_found) +
                " db rows - IT HAS SOME SECONDS DELAY!",
                showlegend=False)
            fig.show()
        else:
            logger.info(symbol + ": No rows found that match criteria")


def analyze_ohlcv(start_date, end_date, delta_dates):
    logger.info("Analyzing ohlcv")

    exchange = "ftx"

    symbols = db_get_all_symbols_from_table(table="rebalancing_ohlcv",
                                            exchange=exchange)

    logger.info("Found these symbols in db: " + str(symbols))

    for symbol in symbols:  # iterate symbols

        # data: holds absolute change of price in between seconds
        data = create_empty_data_structure()

        number_of_rows_found = 0
        for i in range(delta_dates.days + 1):  # iterate all days
            day = start_date + timedelta(days=i)  # current day

            logger.debug("Retrieving ohlcvs for " + str(day.year) +
                         "-" + str(day.month) + "-" + str(day.day))

            # get ohlcvs for the current day for the current symbol
            ohlcvs = db_get_ohlcv_for_day(table="rebalancing_ohlcv",
                                          exchange=exchange, symbol=symbol, timestamp=day, timeframe="15s")
            logger.debug(symbol + ": " + str(len(ohlcvs)) + " rows found")

            # skip if no data for the current day available
            if len(ohlcvs) == 0:
                continue

            # all entries for the current day
            last_time = 0
            last_value = None
            for i, row in enumerate(ohlcvs):
                # print(row)
                # id = row[0]
                # exchange = row[1]
                # symbol = row[2]

                timestamp = row[4]
                minute = timestamp.minute
                second = timestamp.second

                time = str(minute) + ":" + str(second)

                if i != 0:
                    if time in data.keys():
                        if last_time in data.keys():
                            data[time].append(
                                abs(row[8] - last_value))  # close

                last_value = row[8]
                last_time = time

            number_of_rows_found += len(ohlcvs)

        if number_of_rows_found > 0:
            # bring it in shape for scatter plot (collection of xn, yn values including duplicate xn values for the timestamps where length(xn) == length(yn))
            x_values = []
            y_values = []
            for timestamp in data:
                for value in data[timestamp]:
                    x_values.append(timestamp)
                    y_values.append(value)

            fig = px.scatter(x=x_values, y=y_values)

            fig.update_layout(
                title="[" + exchange + "] " +
                "Absolute change in close price for " + symbol + " [15s]" +
                " based on " + str(number_of_rows_found) +
                " db rows - IT HAS SOME SECONDS DELAY!",
                showlegend=False)
            fig.show()
        else:
            logger.info(symbol + ": No rows found that match criteria")


def rebalancing_analysis(parser, args):
    logger.info("Analysing last rebalancings")

    start_date = datetime(year=2021, month=5, day=26)  # first entry in db
    end_date = datetime.utcnow()  # until now
    delta_dates = end_date - start_date

    analyze_prices(start_date, end_date, delta_dates)
    analyze_ohlcv(start_date, end_date, delta_dates)
