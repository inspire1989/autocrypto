from modules.misc.exchange_functions import get_prices_ohlcv
from modules.misc.logger import logger
import modules.misc.mode_selector as mode_selector
import modules.misc.thread_exception_handler as thread_exception_handler
import config.rebalancing_symbols as rebalancing_symbols

from operator import itemgetter
from dateutil import parser
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta
import threading
import pytz
import requests
import json
import traceback


def find_symbols_with_highest_change_since_last_rebalancing(rebalancings, since, now, current_prices):
    result = []

    # find all rebalancings of BULL/BEAR/HEDGE tokens on a PERP token
    for symbol_tuple in rebalancing_symbols.symbol_map:

        # find all rebalancings for the current trade symbol
        rebalancings_on_tradesymbol = []
        for rebalancing in rebalancings:
            if (rebalancing[0] + "/USD" in [symbol for symbol in symbol_tuple["reference"]]) or (rebalancing[0] + "/USDT" in [symbol for symbol in symbol_tuple["reference"]]):
                rebalancings_on_tradesymbol.append(rebalancing)
        logger.debug("Rebalancings on symbol " +
                     str(symbol_tuple) + ": " + ("Not found" if len(rebalancings_on_tradesymbol) == 0 else ""))
        for rebalancing in rebalancings_on_tradesymbol:
            logger.debug("- " + str(rebalancing))

        # now find last rebalancings of BULL/BEAR/HEDGE tokens within this symbol_tuple individually. If a token was rebalanced, this will find both rebalancings for the USD and USDT pair.
        for reference_symbol in symbol_tuple["reference"]:
            last_rebalancing = find_last_rebalancing(
                reference_symbol, rebalancings)

            if last_rebalancing:
                # TODO multi-threaded

                # get close price of the reference symbol at the last rebalancing
                price_at_last_rebalancing = get_prices_ohlcv(when=last_rebalancing[1].replace(second=0, microsecond=0), symbols=[
                                                             reference_symbol], timeframe="1m")[0][1][3]  # close price

                logger.debug(reference_symbol + ": Last rebalancing since " + str(
                    since) + " (" + str(since.tzinfo) + ", yesterday) until " + ("now" if not mode_selector.test_mode else str(now)) + ": " + str(last_rebalancing))
                logger.debug("- Price at rebalancing: $" +
                             str(price_at_last_rebalancing))

                # retrieving current price for this symbol could have failed
                if reference_symbol in [symbol[0] for symbol in current_prices]:
                    current_price = list(
                        filter(lambda x: x[0] == reference_symbol, current_prices))[0][1]  # [3]
                    logger.debug("- Price now: $" +
                                 str(current_price))
                    change = (current_price / price_at_last_rebalancing) - 1
                    logger.debug("--> Change of %.2f%%" % (change*100))

                    # append to results: (refrence symbol, change of price since last rebalancing)
                    result.append((reference_symbol, change))
                else:
                    logger.debug("--> No current price found")
            else:
                logger.debug(reference_symbol + ": No last rebalancing since " +
                             str(since) + " (" + str(since.tzinfo) + ", yesterday) until now found")

    # sort by change
    sorted_result = sorted(result, key=lambda k: abs(k[1]), reverse=True)

    return sorted_result


def find_best_rebalancing_symbol(rebalancings, tickers):
    """
    Finds the symbol that should have the highest price change during the following (aka this) rebalancing.

    With current day being day N:
    - If mode_selector.test_mode, all rebalancings between day N-1 at 0:02:00 and day N at 0:01:999999 are returned.
    - If normal mode, TODO

    :param rebalancings List of all rebalancings.
    :param tickers: Current ticker prices.


    use it like this:
        # get all rebalancings for BULL/BEAR/HEDGE tokens
        rebalancings = get_last_rebalancings(
            rebalancing_symbols.get_reference_symbols(with_suffix=False))

        logger.debug("Last rebalancings ordered by time: ")
        for rebalancing in rebalancings:
            logger.debug("- " + str(rebalancing))

        symbol = find_best_rebalancing_symbol(
            rebalancings=rebalancings, tickers=tickers)

    """
    if mode_selector.test_mode:  # test mode: rebalancings since 0:02 of the day before yesterday and yesterday 0:02
        now = datetime.utcnow().replace(tzinfo=pytz.UTC).replace(
            hour=0, minute=1, second=0, microsecond=0)

        # get current prices for the reference symbols at 0:01:00 of the current day (right before the rebalancing that shall be simulated)
        current_prices_ohlcv = get_prices_ohlcv(
            when=now, symbols=rebalancing_symbols.reference_symbols, timeframe="1m")

        current_prices = []
        for symbol in current_prices_ohlcv:
            current_prices.append((symbol[0], symbol[1][3]))  # close price

        since = now - timedelta(days=1)
    else:  # normal mode: only works if it's currently 0:01:XX. Looks for rebalancings since yesterday 0:02
        now = datetime.utcnow().replace(tzinfo=pytz.UTC)

        since = datetime.utcnow().replace(tzinfo=pytz.UTC).replace(
            second=0, microsecond=0, minute=0) - timedelta(days=1)

        # get prices from ticker
        current_prices = []
        for symbol, content in tickers.items():
            current_prices.append((symbol, content["last"]))

    change_of_symbols_since_last_rebalancing = find_symbols_with_highest_change_since_last_rebalancing(
        rebalancings, since, now, current_prices)

    logger.info(
        "BULL symbols with biggest change in price since last rebalancing:")
    for element in change_of_symbols_since_last_rebalancing:
        if "BULL" in element[0]:
            logger.info("- " + element[0] + ": " +
                        str(round(element[1]*100, 4)) + "%")

    return change_of_symbols_since_last_rebalancing


def get_last_major_rebalancings_single(symbol, url, result, lock, start_time, end_time):
    """
    Get a list of major rebalancings that happened within the day of interest for a single symbol.

    :param symbol Symbol for which rebalancings will be retrieved (without suffix like "/USDT").
    :param url URL to query rebalancings from
    :param result List to append results to
    :param lock: threading.Lock object used to lock the result parameter when appending to it
    :param start_time Only results later than (or equal to) the start_time will be returned
    :param end_time Only results earlier than (or equal to) the end_time will be returned
    :return List of rebalancings (unsorted): [(symbol, utc timestamp, buy side/sell side, [orders], URL from which rebalancing was queried), ...]
            e.g. [('BNBBEAR', datetime object, 'buy', [800.5, 440.9], 'https://ftx.com/api/etfs/rebalance_info), ...]
    """
    try:
        logger.debug("Fetching rebalancings for " + symbol)
        r = requests.get(url, timeout=15)

        r.raise_for_status()  # raise Exception in case request didn't succeed with HTTP 200

        json_data = json.loads(r.text.replace('\\"', '\''))

        if json_data["success"] != True:
            raise Exception("Could not retrieve rebalancings")

        if len(json_data["result"]) == 0:
            raise Exception("No results found in json")

        for rebalancing_info in json_data["result"]:
            # only take rebalancings between start_time and end_time
            if parser.parse(rebalancing_info["time"]).replace(tzinfo=pytz.UTC) <= start_time:
                continue
            elif parser.parse(rebalancing_info["time"]).replace(tzinfo=pytz.UTC) >= end_time:
                continue

            # (symbol, time, side, [sentSize], URL from which rebalancing was queried)
            lock.acquire()
            result.append((symbol.replace("/USDT", "").replace("/USD", ""),
                          parser.parse(rebalancing_info["time"]).replace(
                              tzinfo=pytz.UTC),
                          rebalancing_info["side"],
                          [rebalancing_info["sentSize"]],
                          url))
            lock.release()
    except Exception:
        logger.error(
            "Exception while fetching rebalancings for symbol " + symbol)
        traceback.print_exc()
    finally:
        if lock.locked():
            lock.release()


def get_last_rebalancings_single(symbol, url, result, lock, start_time, end_time):
    """
    Get a list of all rebalancings that happened within the day of interest for a single symbol.

    :param symbol Symbol for which rebalancings will be retrieved (without suffix like "/USDT").
    :param url URL to query rebalancings from
    :param result List to append results to
    :param lock: threading.Lock object used to lock the result parameter when appending to it
    :param start_time Only results later than (or equal to) the start_time will be returned
    :param end_time Only results earlier than (or equal to) the end_time will be returned
    :return List of rebalancings (unsorted): [(symbol, utc timestamp, buy side/sell side, [orders], URL from which rebalancing was queried), ...]
            e.g. [('BNBBEAR', datetime object, 'buy', [800.5, 440.9], 'https://ftx.com/api/etfs/rebalance_info), ...]
    """
    try:
        logger.debug("Fetching rebalancings for " + symbol)
        r = requests.get(url, timeout=15)

        r.raise_for_status()  # raise Exception in case request didn't succeed with HTTP 200

        json_data = json.loads(r.text.replace('\\"', '\''))

        if json_data["success"] != True:
            raise Exception("Could not retrieve rebalancings")

        if len(json_data["result"]) == 0:
            raise Exception("No results found in json")

        for rebalancing_info in json_data["result"]:
            # only take rebalancings between start_time and end_time
            if parser.parse(rebalancing_info["time"]).replace(tzinfo=pytz.UTC) <= start_time:
                continue
            elif parser.parse(rebalancing_info["time"]).replace(tzinfo=pytz.UTC) >= end_time:
                continue
            if rebalancing_info["createdSize"]:
                amount = rebalancing_info["createdSize"]
            elif rebalancing_info["redeemedSize"]:
                amount = rebalancing_info["redeemedSize"]
            else:
                amount = None

            # (symbol, time, side, [sentSize], URL from which rebalancing was queried)
            lock.acquire()
            result.append((symbol.replace("/USDT", "").replace("/USD", ""),
                          parser.parse(rebalancing_info["time"]).replace(
                              tzinfo=pytz.UTC),
                          "Unknown",
                           [amount],
                           url))
            lock.release()
    except Exception:
        logger.error(
            "Exception while fetching rebalancings for symbol " + symbol)
        traceback.print_exc()
    finally:
        if lock.locked():
            lock.release()


def get_last_rebalancings(symbols):
    """
    Get a list of all rebalancings that happened within the day of interest.

    The day of interest is defined as follows (with current day being day N):
    - If mode_selector.test_mode, all rebalancings between day N-1 at 0:02:00 and day N at 0:01:999999 are returned.
    - If normal mode, TODO

    :param symbols Symbols for which rebalancings will be retrieved (without suffix like "/USDT").
    :return List of rebalancings sorted by time: [(symbol, utc timestamp, buy side/sell side, [orders], URL from which rebalancing was queried), ...]
            e.g. [('BNBBEAR', datetime object, 'buy', [800.5, 440.9], 'https://ftx.com/api/etfs/rebalance_info), ...]
    """

    logger.debug("Fetching rebalancings")

    # datetime comparison doesn't work properly with microsecond=0. Hence, use 0:01:999999 instead of 0:02:00
    # start_time and end_time are the same for test mode and normal mode
    start_time = datetime.utcnow().replace(tzinfo=pytz.UTC).replace(
        hour=0, minute=1, microsecond=999999) - timedelta(days=1)

    end_time = datetime.utcnow().replace(tzinfo=pytz.UTC).replace(
        hour=0, minute=1, microsecond=999999)

    # 1. Use api/etfs/rebalance_info to get overview with all symbols (this is the official API)
    result = []
    r = requests.get("https://ftx.com/api/etfs/rebalance_info", timeout=15)

    r.raise_for_status()  # raise Exception in case request didn't succeed with HTTP 200

    json_data = json.loads(r.text.replace('\\"', '\''))
    json_data["result"] = json.loads(json_data["result"].replace('\'', '"'))

    if json_data["success"] != True:
        raise Exception("Could not retrieve rebalancings")

    if len(json_data["result"].items()) == 0:
        raise Exception("No results found in json")

    for rebalancing_info in (json_data["result"].items()):
        # only take rebalancings between start_time and end_time
        if parser.parse(rebalancing_info[1]["time"]).replace(tzinfo=pytz.UTC) <= start_time:
            continue
        elif parser.parse(rebalancing_info[1]["time"]).replace(tzinfo=pytz.UTC) >= end_time:
            continue

        # check if current symbol is present in function's "symbols" parameter
        if rebalancing_info[0] not in symbols:
            continue

        # (symbol, time, side, orderSizeList)
        result.append((rebalancing_info[0],
                       parser.parse(rebalancing_info[1]["time"]).replace(
                           tzinfo=pytz.UTC),
                      rebalancing_info[1]["side"],
                      rebalancing_info[1]["orderSizeList"],
                      "https://ftx.com/api/etfs/rebalance_info"))

    # 2. Query inofficial API api/etfs/SYMBOL for all symbols in background threads
    with ThreadPoolExecutor(len(symbols)*2) as e:
        lock = threading.Lock()
        for symbol in symbols:
            url = "https://ftx.com/api/etfs/" + \
                symbol + "/major_rebalances"
            e.submit(get_last_major_rebalancings_single, symbol, url, result, lock, start_time, end_time).add_done_callback(
                thread_exception_handler.thread_exception_check_callback)

            # this URL provides weird data with empty fields :<
            url = "https://ftx.com/api/etfs/" + \
                symbol.replace(
                    "/USDT", "").replace("/USD", "") + "/rebalances"
            e.submit(get_last_rebalancings_single, symbol, url, result, lock, start_time, end_time).add_done_callback(
                thread_exception_handler.thread_exception_check_callback)

    # make sure the rebalancings don't contain "/USD(T)". This would cause errors later.
    for rebalancing in result:
        if "/USD" in rebalancing[0]:
            raise Exception(
                "List of rebalancings contains symbols with a \"/USD\" or \"/USDT\" pair suffix!")

    # sort results by time (new to old)
    result = sorted(result, key=itemgetter(1), reverse=True)

    # filter duplicates (which happen)
    filtered_result = [result[x]
                       for x in range(len(result)) if not(result[x] in result[:x])]

    return filtered_result


def find_last_rebalancing(symbol, rebalancings):
    """
    Find the last rebalancing of a symbol in a list of rebalancings.
    :param symbol Symbol name to search
    :param rebalancings List of rebalancings, must be sorted by time (descending).
    :return Last rebalancing of the given symbol in the list of rebalancings. None if no rebalancing for the symbol was found.
    """

    # make sure the rebalancings are sorted by time
    if not all(rebalancings[i][1] >= rebalancings[i+1][1] for i in range(len(rebalancings)-1)):
        raise Exception("Rebalancing list must be sorted by time!")

    found = None
    for rebalancing in rebalancings:
        clean_symbol_name = symbol.replace(
            "/USDT", "").replace("/USD", "")
        if rebalancing[0] == clean_symbol_name:
            found = rebalancing
            break

    return found
