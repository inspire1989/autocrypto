from modules.misc.misc import get_root_folder
import os
import datetime
from modules.misc.logger import logger
import dateparser

import binance
from binance.client import Client

import pandas as pd

#import matplotlib.pyplot as plt
from plotly.subplots import make_subplots
import plotly.graph_objs as go


def download_one_shot(client, args):
    # without progress...can take long to succeed
    return client.get_historical_klines(args.coin.replace('_', ''), args.interval, args.start, args.end, limit=1000)


def download_multi_shot(client, args):
    start_unix = binance.helpers.date_to_milliseconds(
        str(datetime.datetime.strptime(args.start, '%Y-%m-%d')))
    end_unix = binance.helpers.date_to_milliseconds(
        str(datetime.datetime.strptime(args.end, '%Y-%m-%d')))

    timestamps = []

    limit = 1000
    if (end_unix - start_unix) / float(binance.helpers.interval_to_milliseconds(args.interval)) > limit:
        timestamps.append(start_unix)
        i = 1
        while True:
            next_timestamp = start_unix + i * limit * \
                int(binance.helpers.interval_to_milliseconds(args.interval))
            if next_timestamp < end_unix:
                timestamps.append(next_timestamp)
                i = i + 1
            else:
                timestamps.append(end_unix)
                break
    else:
        timestamps.append(start_unix)
        timestamps.append(end_unix)

    # check if timestamps correct
    for i in range(0, len(timestamps)):
        if i + 1 < len(timestamps) - 1:
            if timestamps[i + 1] != timestamps[i] + limit * int(
                    binance.helpers.interval_to_milliseconds(args.interval)):
                raise Exception("Timestamp order corrupt! timestamps[" + str(i) + "] = " + str(
                    timestamps[i]) + ", timestamps[" + str(i + 1) + "] = " + str(timestamps[i + 1]))

    logger.info("Requesting " + str(len(timestamps) - 1) + " data sets")
    klines = []
    for i in range(len(timestamps) - 1):
        logger.info("Progress: " +
                    str(round(i / (len(timestamps) - 1) * 100)) + " %")
        klines_tmp = client.get_historical_klines(args.coin.replace('_', ''), args.interval, timestamps[i], timestamps[i + 1],
                                                  limit=limit)
        if i + 1 == len(timestamps) - 1:
            # the last one also contains one entry to include the end date kline
            klines.extend(klines_tmp)
        else:
            klines.extend(klines_tmp[:-1])

    logger.info("Download finished")

    return klines


def binance_download(config, parser, args):
    if args.coin is None:
        parser.error("Download mode requires coin specified with --coin=X_Y.")
    if '_' not in args.coin:
        parser.error(
            "Coin name could not be recognized because it doesn't contain an underscore. Allowed is format like \"BTC_USDT\".")
    if args.interval is None:
        parser.error(
            "Download mode requires interval specified with -i INTERVAL.")
    if args.start is None or args.end is None:
        parser.error(
            "Download mode requires start and end time specified with -s START and -e END.")

    if dateparser.parse(args.start) > dateparser.parse(args.end):
        logger.critical("Start time is bigger than end time!")
        exit(1)

    logger.info("Downloading data for " + args.coin)

    client = Client(config.api_key, config.api_secret)

    klines = download_multi_shot(client, args)
    #klines2 = download_one_shot(client, args)

    logger.info("Finished with " + str(len(klines)) + " klines")

    # Convert to pandas dataframe
    df = pd.DataFrame(klines, dtype=float, columns=('Open time',
                                                    'Open',
                                                    'High',
                                                    'Low',
                                                    'Close',
                                                    'Volume',
                                                    'Close time',
                                                    'Quote asset volume',
                                                    'Number of trades',
                                                    'Taker buy base asset volume',
                                                    'Taker buy quote asset volume',
                                                    'Ignore'))

    df['Open time'] = pd.to_datetime(
        df['Open time'], unit='ms').dt.strftime('%Y-%m-%d %H:%M')
    df['Close time'] = pd.to_datetime(
        df['Close time'], unit='ms').dt.strftime('%Y-%m-%d %H:%M')
    df.set_index('Open time')

    # Save as csv
    output_folder = get_root_folder() + '/history'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    output_file = output_folder + '/' + (args.coin + '_' + args.interval + '_' + str(pd.to_datetime(
        args.start).strftime('%Y-%m-%d %H:%M')) + '_' + str(pd.to_datetime(args.end).strftime('%Y-%m-%d %H:%M')) + '.csv')
    df.to_csv(output_file)

    logger.info("Data stored at " + str(output_file))

    if args.show:
        #df.plot(x='Open time', y=['Close', 'Volume'], subplots=True, title=args.coin + ' from ' + args.start + ' until ' + args.end + ' [' + args.interval + ']')
        # plt.show()

        fig = make_subplots(rows=2, cols=1)
        fig.add_trace(go.Scatter(x=df['Open time'],
                                 y=df['Close'],
                                 name=args.coin + ' from ' + args.start + ' until ' +
                                 args.end + ' [' + args.interval + '] - Close',
                                 mode='lines'),
                      row=1, col=1)
        fig.add_trace(go.Scatter(x=df['Open time'],
                                 y=df['Volume'],
                                 name=args.coin + ' from ' + args.start + ' until ' +
                                 args.end +
                                 ' [' + args.interval + '] - Volume',
                                 mode='lines'),
                      row=2, col=1)
        fig.show()
