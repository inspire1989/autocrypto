
class WhaleAlertConfig():
    def __init__(self):
        print("Of course, the chosen API key doesn't work ;)")
        self.api_key = 'i3bj0jeRLfxAT7Yp4Lc9CPV1mvDpvbEi'

        # free: 10
        # personal: 60
        self.api_calls_per_minute = 60

        # whitelist of addresses to show if address filter is enabled
        # 100 biggest whales according to https://bitinfocharts.com/top-100-richest-bitcoin-addresses.html
        self.address_whitelist = {
            'btc': [
                '35hK24tcLEWcgNA4JxpvbkNkoAcDGqQPsP',  # Huobi-coldwallet
                '37XuVSEpWW4trkfmvWzegTHQt7BdktSKUs',
                '34EiJfy4jGF32M37aQ2ZobupwiRQWa1Siy',
                '1FeexV6bAHb8ybZjqQMjJrcCrHGW9sb6uF',
                '34xp4vRoCGJym3xR7yCVPFHoCNxv4Twseo',  # Binance-coldwallet
                '3D8qAoMkZ8F1b42btt2Mn5TyN7sWfa434A',
                '1HQ3Go3ggs8pFnXuHVHRytPCq5fGG8Hbhx',
                '37tRFZw7n94Jddq6TfVs3MbCXmDX6eMfeY',
                '3JurbUwpsAPqvUkwLM5CtwnEWrNnUKJNoD',
                'bc1qgdjqv0av3q56jvd82tkdjpy7gdp9ut8tlqmgrpmv24sq90ecnvqqjwvw97',  # Bitfinex coldwallet
                '1P5ZEDWTKTFGxQjZphgWPQUpe554WKDfHQ',
                '385cR5DM96n1HvBDMzLHPYcw89fZAXULJP',  # Bittrex-coldwallet
                '3Kzh9qAqVWQhEsfQz7zEQL1EuSx5tyNLNS',  # Coinbase
                '1LdRcdxfbSnmCYYNdeYpUnztiYzVfBEQeC',
                '386eAUqL3ZNZPmHeABXLo658DTQuJeLzUR',  # Binance-coldwallet
                '1AC4fMwgY8j9onSbXEWeH6Zan8QGMSdmtA',
                '38UmuUqPCrFmQo4khkomQwZ4VbY2nZMJ67',
                '1LruNZjwamWJXThX2Y8C2d47QqhAkkc5os',
                'bc1q5shngj24323nsrmxv99st02na6srekfctt30ch',
                '1BX5MXZ95rMiQJBH9yKpcmbAt9VcfyAfHE',
                '17hf5H8D6Yc4B7zHEg3orAtKn7Jhme7Adx',
                '3LCGsSmfr24demGvriN4e3ft8wEcDuHFqh',  # Coincheck
                '13JQwoSLLR3ffXwswe2HCTK9oq4i8MWK3q',
                '12ib7dApVFvg82TXKycWBNpN8kFyiAN1dr',
                '12tkqA9xSoowkzoERHMWNKsTey55YEBqkv',
                '3H5JTt42K7RmZtromfTSefcMEFMMe18pMD',
                '3Pja5FPK1wFB9LkWWJai8XYL1qjbqqT9Ye',
                '1NH8vuaJaMXbtj4Qx6iFaQY8btdVcAn9iz',
                '1ANjYHibCQ6FzagLfeXubC8SQYDfUS5wAJ',
                '1AnwDVbwsLBVwRfqN2x9Eo4YEJSPXo2cwG',  # Kraken.com
                '1932eKraQ3Ad9MeNBHb14WFQbNrLaKeEpT',
                '14eQD1QQb8QFVG8YFwGz7skyzsvBLWLwJS',  # Kraken.com
                '3EcoyfYvsBXBwdbdf4SY6DAU84kqS6mNLs',
                '3HroDXv8hmzKRtaSfBffRgedKpru8fgy6M',
                '1aXzEKiDJKzkPxTZy9zGc3y1nCDwDPub2',
                '17rm2dvb439dZqyMe2d4D6AQJSgg6yeNRn',
                '1PeizMg76Cf96nUQrYg8xuoZWLQozU5zGW',
                '19iqYbeATe4RxghQZJnYVFU4mjUUu76EA6',
                '3JPauiFhP4K3AevmgNMFfMSN7pLZbuADJQ',
                '3M219KR5vEneNb47ewrPfWyb5jQ2DjxRP6',  # Binance-coldwallet
                '3JSssZCJ3PHEdgNv7hzNE26EEzzvYNjZLy',
                '3EiEN1JJCudBgAwL7c6ajqnPzx9LrK1VT6',
                '3BMEXqGpG4FxBA1KWhRFufXfSTRgzfDBhJ',  # Bitmex-coldwallet
                '1GR9qNz7zgtaW5HwwVpEJWMnGWhsbsieCG',
                '35ULMyVnFoYaPaMxwHTRmaGdABpAThM4QR',
                '3N9an8wv4SYi3FVXs3xR5k2AqXeNZiw2mf',
                '38Md7BghVmV7XUUT1Vt9CvVcc5ssMD6ojt',
                '1KUr81aewyTFUfnq4ZrpePZqXixd59ToNn',
                '3DwVjwVeJa9Z5Pu15WHNfKcDxY5tFUGfdx',  # Coinbase
                '3FrM1He2ZDbsSKmYpEZQNGjFTLMgCZZkaf',
                '1BZaYtmXka1y3Byi2yvXCDG92Tjz7ecwYj',
                '1U3d67BaLpwudFEZRtDDAm7bTCQkygUcV',
                '3NmTt7XQmR3S9sPQnbfqSFjKb3gb2uzMLt',
                '1DWxysF7GPRYGShNxL5ux2N2JLRa9rbE6k',
                '1Cr7EjvS8C7gfarREHCvFhd9gT3r46pfLb',
                '3R1hBCHURkquAjFUv1eH5u2gXqooJkjg4B',
                '16FSBGvQfy4K8dYvPPWWpmzgKM6CvrCoVy',
                '1KeK2uTAe8hDVKTbpyDDzd7qfRZ8z3LJx',
                '1HBM45n214sV9yXoizBwTksUgEysTPpk46',
                '12n3s8MCqdZzPnTisYrXagbfw8pJg8y9BW',
                '1MVo3EXLakJym29CFK6o1MyaCBMdvcmmrL',
                '14Ai5GcasUdr5hR2GMzeojkzB9cm4oufHt',
                '1Q7cu7WkeDurYgffeEc9CEnA6zLohbh9iQ',
                '1FUBESNxB2JkyXPc4o9wwoGt158DC9A8dj',
                '16Azr3MAzMKMPmxZXfRsBbBPYHnp2CuJNJ',
                '1JpiTWauQdtysbynNp88dWeuyg2gBbKDcT',
                '1EDRfeNkjkH2SAhSbEKzhKuabnEbVWbKEp',
                '1JYbzYitYQ1ZWTx5KEx7WH2AejC5i5UUbF',
                '1Enhkd9LkQV56a9M12P4VuMDkjyTeLJy5m',
                '1Hyh53PULY6Jyq5LPAJ4CHjgzEbVaqy7KU',
                '1MqAfNMgbZoKtRinT89q1faSZqTKTqCFhR',
                '14YN9Zsx4H8DXUoK2XfRvjuanfjK1RrkHz',
                '1Cb1G5qFK91fShyaPPZWVFwYFBtqRG7h8D',
                '19Te6hzGFSbryomVYqzG2kpBmAJYykx5Yv',
                '1NNGdZMYsN9pgLSXZ5dD5KFUbatFynpmvY',
                '1C5TB2QzeDDJUE4EQD17NmSyEXTk34huRo',
                '1C1bbDApniTd7DtUodpr3ayXxVtcHvwWgx',
                '1LyTftu54VMYCv5pq3S4pMzPRMnsYKTESw',
                '17YyZSNFt31pzGXfZtrzs7Y5Nd56rG2uU5',
                '1q9kwzJggw6AbQjzJeYQFYK8D5gQK8sSh',
                '3DVJfEsDTPkGDvqPCLC41X85L1B1DQWDyh',  # Coinbase
                '14zukxqLuLXg5kqZYRB1Q3Audec3NtxJPW',
                '19G5kkYvjawZiYKhFeh8WmfBN31pdP94Jr',
                '17rfparnM8RaaUyuHFNC9ErqkvRqebNPjE',
                '1F34duy2eeMz5mSrvFepVzy7Y1rBsnAyWC',
                '3AWpzKtkHfWsiv9RGXKA3Z8951LefsUGXQ',
                '34Jpa4Eu3ApoPVUKNTN2WeuXVVq1jzxgPi',
                '1f1miYFQWTzdLiCBxtHHnNiW7WAWPUccr',
                '3FuqMT3VeyRhB917aiJUeWSn2jRt4M3hw8',
                '3E5B5QbDjUL471PEed9vZDwCSck9btBLkD',
                '14YK4mzJGo5NKkNnmVJeuEAQftLt795Gec',
                '1KbrSKrT3GeEruTuuYYUSQ35JwKbrAWJYm',
                '1P1iThxBH542Gmk1kZNXyji4E4iwpvSbrt',
                '12tLs9c9RsALt4ockxa1hB4iTCTSmxj2me',
                'bc1q2raxkmk55p000ggfa8euzs9fzq7p4cx4twycx7',
                '1Ki3WTEEqTLPNsN5cGTsMkL2sJ4m5mdCXT',
                '1BAFWQhH9pNkz3mZDQ1tWrtKkSHVCkc3fV',
                '1ucXXZQSEf4zny2HRwAQKtVpkLPTUKRtt',
                '1CPaziTqeEixPoSFtJxu74uDGbpEAotZom',
                '33QoG5ioV4hseifKT9iaqrmD2eis7DicWA'
            ]
        }
