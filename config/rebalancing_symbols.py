# Enable/disable the symbol whitelist
whitelist_enabled = False

# Use /don't use the USDT coin pairs. If enabled, only the normal USD token pairs will be used which often have much more volume.
usdt_symbols_enabled = False


# will be filled by set_symbols method
symbol_map = [
    # {'tradesymbol': 'LINK-PERP',
    # 'reference': ['LINKBULL/USD']},

    # {'tradesymbol': 'BTC-PERP',
    # 'reference': ['BULL/USD']}
]

symbol_whitelist = ["LINK-PERP",  # great
                    "SUSHI-PERP",  # great
                    "ADA-PERP",  # great
                    "XRP-PERP",  # awesome
                    "DOGE-PERP",  # great
                    # "BNB-PERP",  # okay
                    # "ETH-PERP",  # okay
                    # "VET-PERP",  # okay
                    # "BCH-PERP",  # good
                    # "LTC-PERP",  # good
                    "SXP-PERP",  # great
                    "COMP-PERP",  # great
                    "ATOM-PERP",  # awesome
                    # "XTZ-PERP",  # good

                    # THERE ARE MANY MORE THAT I DIDN'T CHECK!
                    ]

# array of all mentioned symbols from the symbol_map
all_symbols = []

# array of all reference symbols from the symbol_map
reference_symbols = []


def get_reference_symbols(with_suffix):
    """
    Return a list of all reference symbols.
    :param with_suffix: True if returned symbol names shall have "/USD(T)" suffixes, False if not.
    :return list of reference symbols
    """
    result = []

    for pair in symbol_map:
        for reference_symbol in pair["reference"]:
            if with_suffix:
                result.append(reference_symbol)
            else:
                result.append(reference_symbol.replace(
                    "/USDT", "").replace("/USD", ""))
    return result


def clear_symbols():
    """
    Clear all known symbols.
    """
    global symbol_map
    symbol_map = []

    global all_symbols
    all_symbols = []

    global reference_symbols
    reference_symbols = []


def set_symbols(symbols_with_etf_tokens):
    """
    Inserts symbols into the symbol_map if it's trade-symbol is listed in the symbol_whitelist. Also updates all_symbols.
    :param symbols_with_etf_tokens: List of tuples ("trade-symbol", "reference"). E.g. ("LINK-PERP", "LINKBULL/USD")
    """
    global symbol_map
    global symbol_whitelist
    for symbol_pair in symbols_with_etf_tokens:
        if symbol_pair[0] in symbol_whitelist or not whitelist_enabled:

            # check if trade symbol is already present in symbol_map
            found = False
            found_index = -1
            for index, element in enumerate(symbol_map):
                if element["tradesymbol"] == symbol_pair[0]:
                    found = True
                    found_index = index

            if not usdt_symbols_enabled:
                if symbol_pair[1].endswith("USDT"):
                    continue

            if found:
                symbol_map[found_index]["reference"].append(symbol_pair[1])
            else:
                symbol_map.append(
                    {"tradesymbol": symbol_pair[0], "reference": [symbol_pair[1]]})

    global all_symbols
    all_symbols = [symbol_dict['tradesymbol'] for symbol_dict in symbol_map] + \
        [symbol for reference_list in symbol_map for symbol in reference_list['reference']]

    global reference_symbols
    reference_symbols = [
        symbol for reference_list in symbol_map for symbol in reference_list['reference']]
